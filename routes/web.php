<?php

$currentLangCount = App\Language::where('code', Request::segment(1))->where('deleted', 'no')->where('status', 'active')->count();
if ($currentLangCount == 1) {
	$currentLang = App\Language::where('code', Request::segment(1))->where('deleted', 'no')->where('status', 'active')->first();
}else{
	$currentLang = App\Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
}

$domainSlug = '';
foreach (config('domains') as $key => $value) {
	if($value == parse_url(URL::current(),PHP_URL_SCHEME).'://'.parse_url(URL::current(),PHP_URL_HOST)){
		$domainSlug = $key;
	}
}

if ($currentLangCount != 1) {
	header('HTTP/1.1 301 Moved Permanently');
	header('Location: /'.$currentLang->code.'/'.$domainSlug);
	exit();
}

if(App\MenuVariable::where('slug', Request::segment(2))->count() >= 1){
	$dDetect = App\MenuVariable::where('slug', Request::segment(2))->first()->menu;
	while ($dDetect->topMenu != null) {
	    $dDetect = $dDetect->topMenu;
	}

	if(is_null(config('domains.'.$dDetect->variableLang($currentLang->code)->slug))){
		$domain = config('domains.cin-ali');
	}else{
		$domain = config('domains.'.$dDetect->variableLang($currentLang->code)->slug);
	}

	if (!str_contains(URL::current(), $domain)) {
		header('Location: http://'.$domain.'/'.Request::segment(1).'/'.Request::segment(2));
	}
}

Route::post('/{lang}/form_save', 'HomeController@form_save');
Route::post('/{lang}/etkinlik_odeme_yap/{calendarid}', 'HomeController@etkinlik_odeme_yap');
Route::get('/{lang}/map', function () {return view('partials.map');});

//Route::redirect('/', '/'.$currentLang->code.'/'.$mainMenu->variable->slug, 301);
Route::get('/{lang}/{slug}/{attr?}/{param?}', 'HomeController@index');
Route::post('/{lang}/{slug}/{attr?}/{param?}', 'HomeController@index');