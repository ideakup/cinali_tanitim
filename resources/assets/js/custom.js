$("#fiyat-1").ionRangeSlider({
    type: "double",
    grid: false,
    min: 0,
    max: 1000,
    from: 200,
    to: 800,
    prefix: "₺"
});

jQuery(document).ready(function() {

    var btn = $('#topBtn');
    //$('html, body').animate({ scrollTop: 0 }, '0');

    $(window).scroll(function() {
    	if($(window).scrollTop() >= $('.headerLogoContainer').outerHeight()) {
			$('.navbar').addClass('navbar-fixed-top');
			$('.headerLogoContainer').hide();
			$('.menuSpacer').show();
		}else{
			$('.navbar').removeClass('navbar-fixed-top');
			$('.headerLogoContainer').show();
			$('.menuSpacer').hide();
		}

        if ($(window).scrollTop() > 300) {
            btn.show();
        } else {
            btn.hide();
        }
    });

    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, '300');
    });

});