	<style>
		.form-control{
			height: 39px;
			border-radius: 2px;
		}

		textarea.form-control {
		    height: 93px;
		}

		.iletisim p{
			text-align: left;
			font-size: 14px;
		}

		.iletisimHead{
			background-image: url('{{ url('images/pul.png') }}');
		    margin-bottom: 15px;
		    background-position: right center;
		    background-repeat: no-repeat;
		    background-size: contain;
		}

		.iletisimHead h2{
			color: #FFF; margin: 18px 0 12px; line-height: 30px;
		}
	</style>

	<div class="col-md-12 m-b-20" style="background-color: #e9f4f9;">
		
		<div class="col-md-3">
			<h1>Cin Ali</h1>
			{!! $menu->content[0]->variableLang(Request::segment(1))->content !!}
		</div>

		<div class="col-md-9">

			<script type="text/javascript">
				var formData_{{ $menu->content[1]->id }} = '{!! $menu->content[1]->variableLang(Request::segment(1))->content !!}';
			</script>

			<style type="text/css">
				.iletisim_formm .form-group > label{
					display: none;
				}

				.iletisim_formm .form_container .form-group{
					width: 50%;
					padding: 0 15px;
					display: inline-block;
					
				}

				.iletisim_formm .form_container .form-group input.form-control{
					height: 39px;
    				border-radius: 2px;
				}

				.iletisim_formm .form_container .form-group textarea.form-control{
    				border-radius: 2px;
				}

				.iletisim_formm .btn.ladda-button{
					margin: 0 15px 15px 15px;
				}
			</style>

			<form class="iletisim_formm" id="form_{{ $menu->content[1]->id }}" method="POST" action="{{ url('tr/form_save') }}">
				{{ csrf_field() }}
				<input type="hidden" name="lang" value="{{ Request::segment(1) }}">
				<input type="hidden" name="source_type" value="menu">
				<input type="hidden" name="source_id" value="{{ $menu->id }}">
				<input type="hidden" name="form_id" value="{{ $menu->content[1]->id }}">

				<div class="col-md-12">
					<p style="margin-top: 37px;">İletişim Formu</p>
				</div>

				<div class="form_container" id="form_element_{{ $menu->content[1]->id }}"></div>

				<button type="submit" id="form_submit_{{ $menu->content[1]->id }}" class="btn ladda-button" data-color="first" data-style="expand-right" data-size="s" style="outline: 0;"><span class="ladda-label"><i class="fa fa-paper-plane" aria-hidden="true"></i> Gönder </span><span class="ladda-spinner"></span></button>

			</form>

		</div>

	</div>
	
	<div class="col-md-3 iletisim m-b-20">
		<div class="col-md-12 iletisimHead" style="background-color: #fed630;"> <h2>Müze</h2> </div>
		<div class="col-md-12">
			{!! $menu->content[2]->variableLang(Request::segment(1))->content !!}
		</div>
	</div>
	<div class="col-md-3 iletisim m-b-20">
		<div class="col-md-12 iletisimHead" style="background-color: #feb555;"> <h2>Vakıf</h2> </div>
		<div class="col-md-12">
			{!! $menu->content[3]->variableLang(Request::segment(1))->content !!}
		</div>
	</div>
	<div class="col-md-3 iletisim m-b-20">
		<div class="col-md-12 iletisimHead" style="background-color: #be6f7e;"> <h2>Kütüphane</h2> </div>
		<div class="col-md-12">
			{!! $menu->content[4]->variableLang(Request::segment(1))->content !!}
		</div>
	</div>
	<div class="col-md-3 iletisim m-b-20">
		<div class="col-md-12 iletisimHead" style="background-color: #c6e04e;"> <h2>Filli Bahçe</h2> </div>
		<div class="col-md-12">
			{!! $menu->content[5]->variableLang(Request::segment(1))->content !!}
		</div>
	</div>
	
	<div class="col-md-12">
		<iframe src="{{URL::to('tr/map')}}" width="100%" height="300" frameborder="0"></iframe>
	</div>
