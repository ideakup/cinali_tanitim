<div class="row home-main hidden-xs">
    <div class="col-xs-12">
        <a href="{{ config('domains.muzesi').'/'.$lang.'/muzesi' }}" class="button red"><span>MÜZESİ</span>MÜZESİ</a>
        <a href="{{ config('domains.vakfi').'/'.$lang.'/vakfi' }}" class="button orange"><span>VAKFI</span>VAKFI</a>
        <a href="{{ config('domains.filli-bahcesi').'/'.$lang.'/filli-bahcesi' }}" class="button green"><span>FİLLİ BAHÇESİ</span>FİLLİ BAHÇESİ</a>
        <a href="{{ config('domains.kutuphanesi').'/'.$lang.'/kutuphanesi' }}" class="button purple"><span>KÜTÜPHANESİ</span>KÜTÜPHANESİ</a>
        <a href="{{ config('domains.cin-ali').'/'.$lang.'/etkinlikler' }}" class="button blue"><span>ETKİNLİKLERİ</span>ETKİNLİKLERİ</a>
        <a href="{{ config('domains.dukkani') }}" target="_blank" class="button yellow"><span>DÜKKANI</span>DÜKKANI</a>
    </div>
</div>