<div class="footer-main">
	<div class="container">
		<div class="row">
			
			@foreach ($menuall as $menuitem)
				@if ($loop->index < 6)
					@if ($loop->index % 3 == 0)
						<div class="col-sm-6 padding-0 foot-menu">
					@endif

						@php
		                    $dDetectMenu = $menuitem;
		                    while ($dDetectMenu->topMenu != null) {
		                        $dDetectMenu = $dDetectMenu->topMenu;
		                    }
		                    if(is_null(config('domains.'.$dDetectMenu->variableLang($lang)->slug))){
		                        $domainMenu = config('domains.cin-ali');
		                    }else{
		                        $domainMenu = config('domains.'.$dDetectMenu->variableLang($lang)->slug);
		                    }
		                @endphp

						<div class="col-sm-4" @if ($loop->index == 0) style="border: 0;" @endif>
			                
			                @if($menuitem->type == 'link')
								
								<h4>
									<a href="{{ json_decode($menuitem->variableLang(Request::segment(1))->stvalue)->link }}" target="_{{ json_decode($menuitem->variableLang(Request::segment(1))->stvalue)->target }}">
					                    {{ $menuitem->variable->menutitle }}
					                </a>
								</h4>

			                @else
								
								<h4>
									<a href="{{ $domainMenu.'/'.$lang.'/'.$menuitem->variableLang(Request::segment(1))->slug }}">
										{{ $menuitem->variableLang(Request::segment(1))->menutitle }}
									</a>
								</h4>

			                @endif
							
							@if ($menuitem->subMenu->count() >= 1)
								<ul>
									@foreach ($menuitem->subMenu as $submenuitem)
										<li>
											@if ($submenuitem->type == 'content' || $submenuitem->type == 'list' || $submenuitem->type == 'photogallery')
								                <a href="{{ $domainMenu.'/'.$lang.'/'.$submenuitem->variableLang($lang)->slug }}">
								                    {{ $submenuitem->variable->menutitle }} {{-- $submenuitem->subMenuTop->count().' + '.$i --}}
								                </a>
								            @elseif ($submenuitem->type == 'link')

								                @php
								                    $linkUrl = '';
								                @endphp
								                @if (starts_with(json_decode($submenuitem->variableLang(Request::segment(1))->stvalue)->link, '#'))
								                    @php
								                        $linkUrl = $submenuitem->topMenu->variableLang(Request::segment(1))->slug.''.json_decode($submenuitem->variableLang(Request::segment(1))->stvalue)->link;
								                    @endphp
								                @else
								                    @php
								                        $linkUrl = json_decode($submenuitem->variableLang(Request::segment(1))->stvalue)->link;
								                    @endphp
								                @endif

								                <a href="{{ $domainMenu.'/'.$lang.'/'.$linkUrl }}" target="_{{ json_decode($submenuitem->variableLang(Request::segment(1))->stvalue)->target }}">
								                    {{ $submenuitem->variable->menutitle }} 
								                </a>

								            @endif
										</li>
									@endforeach
								</ul>
							@endif
													
						</div>

					@if ($loop->index % 3 == 2)
						</div>
					@endif
				@endif
			@endforeach
		</div>
		<div class="row">
			<div class="col-sm-6 padding-0">

				@php
	                $dDetectMenu = $menu;
	                while ($dDetectMenu->topMenu != null) {
	                    $dDetectMenu = $dDetectMenu->topMenu;
	                }
	                if(is_null(config('domains.'.$dDetectMenu->variableLang($lang)->slug)) || $dDetectMenu->variableLang($lang)->slug == 'cin-ali'){
	                    $domainLogo = 'logo';
	                }else{
	                    $domainLogo = 'logo2';
	                }
	            @endphp

				<div class="col-sm-3" style="padding-top: 10px;"><img src="{{asset('images/'.$domainLogo.'.png')}}" class="img-responsive" alt=""></div>
				<div class="col-sm-9" style="font-size: 10px; padding-top: 16px;">					
					@if (!empty($sitesettings->where('slug', 'footer-address')->first()->value))
						{{ $sitesettings->where('slug', 'footer-address')->first()->value }}
                    @endif
					<br>
					@if (!empty($sitesettings->where('slug', 'footer-phone')->first()->value))
						T: {{ $sitesettings->where('slug', 'footer-phone')->first()->value }}
                    @endif

                    @if (!empty($sitesettings->where('slug', 'footer-faks')->first()->value))
						F: {{ $sitesettings->where('slug', 'footer-faks')->first()->value }}
                    @endif

                    @if (!empty($sitesettings->where('slug', 'footer-email')->first()->value))
						{{ $sitesettings->where('slug', 'footer-email')->first()->value }}
                    @endif
				</div>				
			</div>
			<div class="col-sm-6" style="padding-top: 10px;">
				
				<div class="col-sm-5" style="text-align: right; font-size: 12px;">
					Cin Ali E-Bültene Kayıt Olun!<br>Yeniliklerden Haberdar Olun!
				</div>
				<div class="col-lg-7">
					@php
						$kayitFormu = App\Content::find(216);
					@endphp
					<script type="text/javascript">
						var formData_216 = '{!! $kayitFormu->variableLang(Request::segment(1))->content !!}';
					</script>

					<style type="text/css">
						.kayitol_formm .form-group > label{
							display: none;
						}

						.kayitol_formm .form_container .form-group input.form-control{
							border-radius: 0; border: none; height: 30px; font-size: 12px;
						}

						.kayitol_formm .form_container .form-group textarea.form-control{
		    				border-radius: 2px;
						}

					</style>

					<form class="kayitol_formm" id="form_{{ $kayitFormu->id }}" method="POST" action="{{ url('tr/form_save') }}">
						{{ csrf_field() }}
						<input type="hidden" name="lang" value="{{ Request::segment(1) }}">
						<input type="hidden" name="source_type" value="menu">
						<input type="hidden" name="source_id" value="{{ $menu->id }}">
						<input type="hidden" name="form_id" value="{{ $kayitFormu->id }}">

						<div class="form_container" id="form_element_{{ $kayitFormu->id }}" style="width:calc(100% - 80px);float: left;"></div>

						<button type="submit" id="form_submit_{{ $kayitFormu->id }}" class="btn ladda-button" data-color="first" data-style="expand-right" data-size="s" style="outline: 0; font-size: 12px; float: right; width: 80px;"><span class="ladda-label"><i class="fa fa-paper-plane" aria-hidden="true"></i> Gönder </span><span class="ladda-spinner"></span></button>

					</form>
					<!--
					    <div class="input-group">
							<input type="text" class="form-control" placeholder="E-posta adresinizi giriniz..." style="border-radius: 0; border: none; height: 30px;    font-size: 12px;">
							<span class="input-group-btn">
								<button type="submit" id="form_submit" class="btn ladda-button" data-color="first" data-style="expand-right" data-size="s" style="outline: 0; font-size: 12px;"><span class="ladda-label"><i class="fa fa-paper-plane" aria-hidden="true"></i> Kayıt Ol! </span><span class="ladda-spinner"></span></button>
							</span>
						</div>
					-->
				</div>
				<!--<div class="col-sm-3 padding-0"><img src="{{asset('images/visa.png')}}" style="margin-top: 16px;" class="img-responsive" alt=""></div>-->
			</div>
		</div>		
	</div>
</div>