<html>

<head>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
    /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
    
    #map {
        height: 100%;
    }
    /* Optional: Makes the sample page fill the window. */
    
    html,
    body {
        height: 100%;
        margin: 0;
        padding: 0;
    }
    </style>
</head>

<body>
    <div id="map"></div>
    <script>
    var map;

    function initMap() {
        var uluru = {
            lat: 39.905736,
            lng: 32.862874
        };
        map = new google.maps.Map(document.getElementById('map'), {
            center: uluru,
            zoom: 15,
            styles: [ { "featureType": "all", "elementType": "labels.text", "stylers": [ { "lightness": "-8" }, { "hue": "#000bff" }, { "saturation": "-72" } ] }, { "featureType": "all", "elementType": "labels.text.stroke", "stylers": [ { "color": "#ffffff" }, { "weight": "1.00" } ] }, { "featureType": "landscape", "elementType": "geometry.fill", "stylers": [ { "color": "#e4eaeb" } ] }, { "featureType": "landscape.man_made", "elementType": "geometry.fill", "stylers": [ { "color": "#e4eaeb" }, { "saturation": "0" }, { "lightness": "0" } ] }, { "featureType": "landscape.man_made", "elementType": "geometry.stroke", "stylers": [ { "color": "#e4eaeb" }, { "lightness": "-6" } ] }, { "featureType": "landscape.man_made", "elementType": "labels.text.stroke", "stylers": [ { "visibility": "off" } ] }, { "featureType": "landscape.natural", "elementType": "geometry.fill", "stylers": [ { "saturation": "0" }, { "lightness": "0" } ] }, { "featureType": "poi", "elementType": "all", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi.attraction", "elementType": "all", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi.business", "elementType": "all", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi.government", "elementType": "all", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi.medical", "elementType": "all", "stylers": [ { "visibility": "on" } ] }, { "featureType": "poi.medical", "elementType": "geometry.fill", "stylers": [ { "lightness": "-16" } ] }, { "featureType": "poi.park", "elementType": "all", "stylers": [ { "visibility": "on" } ] }, { "featureType": "poi.park", "elementType": "geometry.fill", "stylers": [ { "color": "#a9de83" }, { "lightness": "0" }, { "saturation": "0" } ] }, { "featureType": "poi.park", "elementType": "labels.text", "stylers": [ { "lightness": "24" } ] }, { "featureType": "poi.place_of_worship", "elementType": "all", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi.school", "elementType": "all", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi.sports_complex", "elementType": "all", "stylers": [ { "visibility": "off" } ] }, { "featureType": "road", "elementType": "geometry.fill", "stylers": [ { "saturation": "-100" }, { "lightness": "8" }, { "hue": "#003cff" } ] }, { "featureType": "road", "elementType": "geometry.stroke", "stylers": [ { "lightness": "0" }, { "saturation": "0" }, { "color": "#919ab7" } ] }, { "featureType": "road", "elementType": "labels.text.fill", "stylers": [ { "color": "#767676" } ] }, { "featureType": "road", "elementType": "labels.text.stroke", "stylers": [ { "color": "#ffffff" } ] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [ { "color": "#a0b5be" }, { "lightness": "48" } ] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [ { "color": "#76868c" }, { "lightness": "40" } ] }, { "featureType": "road.highway.controlled_access", "elementType": "geometry.fill", "stylers": [ { "color": "#a0b5be" }, { "lightness": "8" } ] }, { "featureType": "road.highway.controlled_access", "elementType": "geometry.stroke", "stylers": [ { "lightness": "-24" } ] }, { "featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [ { "lightness": "24" } ] }, { "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [ { "lightness": "32" } ] }, { "featureType": "transit.station.airport", "elementType": "geometry.fill", "stylers": [ { "lightness": "0" } ] }, { "featureType": "water", "elementType": "all", "stylers": [ { "saturation": "0" }, { "lightness": "0" }, { "color": "#a6cbe3" } ] } ]
        });
        var marker = new google.maps.Marker({
            position: uluru,
            icon: '{{ asset('images/marker.png') }}',
            map: map
        });
    }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD1ov1TDXv_JIXMh9xZ1GKmr1fLGyO6MKI&callback=initMap" async defer></script>
</body>

</html>
