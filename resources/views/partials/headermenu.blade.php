@if (empty($submenu))
    @if ($menuitem->subMenuTop->count() > 0)
        @php $i++; @endphp
        <li class="dropdown">
            @php
                $dDetectMenuitem = $menuitem;
                while ($dDetectMenuitem->topMenu != null) {
                    $dDetectMenuitem = $dDetectMenuitem->topMenu;
                }
                if(is_null(config('domains.'.$dDetectMenuitem->variableLang($lang)->slug))){
                    $domainMenuitem = config('domains.cin-ali');
                }else{
                    $domainMenuitem = config('domains.'.$dDetectMenuitem->variableLang($lang)->slug);
                }
            @endphp

            <a class="dropdown-toggle" href="@if ($menuitem->type == 'menuitem')#@else{{ $domainMenuitem.'/'.$lang.'/'.$menuitem->variableLang($lang)->slug }}@endif">{{ $menuitem->variable->menutitle }}<i class="fa fa-caret-down"></i></a>

            @if ($menuitem->subMenuTop->count() > 0)
                <ul class="dropdown-menu">
                    @foreach ($menuitem->subMenuTop as $menuitem)
                        @if ($menuitem->position != 'aside')
                            @include('partials.headermenu', ['menuitem' => $menuitem, 'submenu' => true])
                        @endif
                    @endforeach
                </ul>
            @endif
        </li>
    @else
        <li>
            @if ($menuitem->type == 'content' || $menuitem->type == 'list' || $menuitem->type == 'photogallery' || $menuitem->type == 'calendar')

                @php
                    $dDetectMenuitem = $menuitem;
                    while ($dDetectMenuitem->topMenu != null) {
                        $dDetectMenuitem = $dDetectMenuitem->topMenu;
                    }
                    if(is_null(config('domains.'.$dDetectMenuitem->variableLang($lang)->slug))){
                        $domainMenuitem = config('domains.cin-ali');
                    }else{
                        $domainMenuitem = config('domains.'.$dDetectMenuitem->variableLang($lang)->slug);
                    }
                @endphp

                <a href="{{ $domainMenuitem.'/'.$lang.'/'.$menuitem->variableLang($lang)->slug }}">
                    {{ $menuitem->variable->menutitle }}
                </a>
            @elseif ($menuitem->type == 'link')

                <a href="{{ json_decode($menuitem->variableLang(Request::segment(1))->stvalue)->link }}" target="_{{ json_decode($menuitem->variableLang(Request::segment(1))->stvalue)->target }}">
                    {{ $menuitem->variable->menutitle }}
                </a>

            @endif
        </li>
    @endif
@else
    @if ($menuitem->subMenuTop->count() > 0)
        @php $i++; @endphp
        <li class="dropdown-submenu">
            <a href="{{ $domainMenuitem.'/'.$lang.'/'.$menuitem->variableLang($lang)->slug }}" class="dropdown-item dropdown-toggle">
                {{ $menuitem->variable->menutitle }} {{-- $menuitem->subMenuTop->count().' ? '.$i --}} 
            </a>
            @if ($menuitem->subMenuTop->count() > 0)
                <ul class="dropdown-menu @if ($i == 3) dropright-menu @endif">
                    @foreach ($menuitem->subMenuTop as $menuitem)
                        @if ($menuitem->position != 'aside')
                            @include('partials.headermenu', ['menuitem' => $menuitem, 'submenu' => true])
                        @endif
                    @endforeach
                </ul>
            @endif
        </li>
    @else
        @php $i++; @endphp
        <li>
            @if ($menuitem->type == 'content' || $menuitem->type == 'list' || $menuitem->type == 'photogallery' || $menuitem->type == 'photogallery')
                <a href="{{ $domainMenuitem.'/'.$lang.'/'.$menuitem->variableLang($lang)->slug }}">
                    {{ $menuitem->variable->menutitle }} {{-- $menuitem->subMenuTop->count().' + '.$i --}}
                </a>
            @elseif ($menuitem->type == 'link')
                @php
                    $linkUrl = '';
                @endphp
                @if (starts_with(json_decode($menuitem->variableLang(Request::segment(1))->stvalue)->link, '#'))
                    @php
                        $linkUrl = $menuitem->topMenu->variableLang(Request::segment(1))->slug.''.json_decode($menuitem->variableLang(Request::segment(1))->stvalue)->link;
                    @endphp
                @else
                    @php
                        $linkUrl = json_decode($menuitem->variableLang(Request::segment(1))->stvalue)->link;
                    @endphp
                @endif

                <a href="{{  $domainMenuitem.'/'.$lang.'/'.$linkUrl }}" target="_{{ json_decode($menuitem->variableLang(Request::segment(1))->stvalue)->target }}">
                    {{ $menuitem->variable->menutitle }} 
                </a>
            @endif
        </li>
    @endif
@endif