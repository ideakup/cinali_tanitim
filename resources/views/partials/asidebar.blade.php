@if ($menu->asidevisible == 'yes')
	@if ($menu->subMenu->where('position', '!=', 'top')->count() > 0)
		<div class="container m-b-20">
			<div class="row">
				<ul class="nav navbar-nav">
		    		@foreach ($menu->subMenu->where('position', '!=', 'top') as $subMenu)
		    			<li class="nav-item">
						    <a class="nav-link" href="
						    	@if ($subMenu->type == 'link')
				    				{{ json_decode($subMenu->variableLang(Request::segment(1))->stvalue)->link }}
				    			@else
				    				{{ url($lang.'/'.$subMenu->variableLang(Request::segment(1))->slug) }}
				    			@endif" @if ($subMenu->type == 'link')
				    				target="_{{ json_decode($subMenu->variableLang(Request::segment(1))->stvalue)->target }}"
								@endif
							>{{$subMenu->variableLang(Request::segment(1))->menutitle}}</a>
						</li>
		    		@endforeach
				</ul>
		    </div>
		</div>
	@endif
@endif