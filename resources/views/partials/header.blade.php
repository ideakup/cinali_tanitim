<nav class="navbar navbar-default">
    <div class="container padding-0 headerLogoContainer">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            @php
                $dDetectMenu = $menu;
                while ($dDetectMenu->topMenu != null) {
                    $dDetectMenu = $dDetectMenu->topMenu;
                }
                if(is_null(config('domains.'.$dDetectMenu->variableLang($lang)->slug))){
                    $domainLogo = 'cin-ali';
                }else{
                    $domainLogo = $dDetectMenu->variableLang($lang)->slug;
                }
            @endphp

            <a class="navbar-brand" href="/{{ $lang.'/'.$headerslug }}/"><img src="{{asset('images/logo/'.$domainLogo.'Logo2.png')}}" class="img-responsive" alt="Cin Ali"></a>
            <div class="navbar-right">             
            </div>
        </div>
    </div>
    <div class="collapse navbar-collapse padding-0" id="bs-example-navbar-collapse-1">
        <div class="main-fix-menu">
            <div class="container">
                <style type="text/css">
                    .dropdown-toggle .fa-caret-down::before{
                        content: "";
                    }
                </style>
                <ul class="nav navbar-nav">
                    @php 
                        $i = 0; 
                    @endphp
                    @foreach ($topmenus as $menuitem)
                        @include('partials.headermenu')
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</nav>
<div class="menuSpacer" style="height: 212px; display:none;"></div>