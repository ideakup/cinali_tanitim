@if ($menu->breadcrumbvisible == 'yes')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @php
                    $breadcrumbMenuArr = array();
                    $breadcrumbMenuArr[] = $menu;
                    $breadcrumbMenu = $menu;

                    while ($breadcrumbMenu->topMenu != null) {
                        $breadcrumbMenu = $breadcrumbMenu->topMenu;
                        $breadcrumbMenuArr[] = $breadcrumbMenu;
                    }

                    $bcArr = array_reverse($breadcrumbMenuArr);

                @endphp
                <ol class="breadcrumb">
                    @foreach ($bcArr as $element)
                        <li class="breadcrumb-item"><a href="{{ url($lang.'/'.$element->variableLang(Request::segment(1))->slug) }}">{{ $element->variableLang(Request::segment(1))->menutitle }}</a></li>

                        @if ($loop->last && !is_null($attr))
                            {{-- 'content','list','link','photogallery','calendar' --}}
                           
                            @if ($element->type == 'photogallery' || $element->type == 'list' )
                                <li class="breadcrumb-item">
                                    <a href="{{ url($lang.'/'.Request::segment(2).'/'.$content->variableLang(Request::segment(1))->slug) }}">
                                        {{ $content->variableLang(Request::segment(1))->title }}
                                    </a>
                                </li>
                            @endif
                        @endif
                    @endforeach
                        
                </ol>
            </div>
        </div>
    </div>
    
@endif
<div class="breadcrumbSeperator"> </div>