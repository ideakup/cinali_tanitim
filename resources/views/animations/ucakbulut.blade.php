<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="ucakbulut">
                <div class="home-banner">
                	<div id="block" class="col-xs-12" style="height: 140px;">
                		<img id="cloud_1" class="img-responsive" style="top: 10%; left: 70%; height: 95px;" src="{{asset('images/animation/cloud.png')}}" alt="">
                		<img id="cloud_3" class="img-responsive" style="top: 40%; left: 50%; height: 70px;" src="{{asset('images/animation/cloud.png')}}" alt="">
                		<img id="cloud_2" class="img-responsive" style="top: 30%; left: 35%; height: 50px;" src="{{asset('images/animation/cloud.png')}}" alt="">
                		<img id="cloud_4" class="img-responsive" style="top: 20%; left: 15%; height: 20px;" src="{{asset('images/animation/cloud.png')}}" alt="">
                		<img id="planeOnFlag" class="img-responsive" style="top: 30%; left: 30%;" src="{{asset('images/animation/planeOnFlag.png')}}" alt="">
                	</div>
                </div>
                <script type="text/javascript">
                    var ucakbulutanim = function(){
                        //bulutlar
                        for($i=1;$i<5;$i++){
                            var cl = new TimelineMax();
                            cl.fromTo($('#cloud_'+$i), 14*$i, {x:'0', ease:Power0.easeNone}, {x: '+=1500', ease:Power0.easeNone});
                            cl.fromTo($('#cloud_'+$i), 14*$i, {x:'-=1500', ease:Power0.easeNone}, {x: '0', ease:Power0.easeNone});
                            cl.repeat(-1);  
                        }           
                        //plane
                        var pl = new TimelineMax();
                        pl.fromTo($('#planeOnFlag'), 8, {x:'0', y:'0'}, {x: '-=1500', y: '0', ease:Sine.easeIn});
                        pl.fromTo($('#planeOnFlag'), 8, {x:'+=1500', y: '0', ease:Sine.easeOut}, {x: '0', y:'0'});
                        pl.repeat(0);
                        pl.repeatDelay(2);
                    }
                </script>
            </div>
        </div>
    </div>
</div>


               
     