<div class="container">
    <div class="row" style="min-height: 130px;">
    	<div class="col-md-5">
    		<div class="cark_back">
    			<div class="cark"></div>
    		</div>
    	</div>
    </div>
</div>
<script type="text/javascript">
    var carkanim = function(){
	  	var cr = new TimelineMax();
	  	cr.from($('.cark'), 4, {rotation:-360, transformOrigin:"50% 50%", ease:Expo.easeInOut});
	  	cr.repeat(-1);
	  	cr.repeatDelay(3);
	}
</script>