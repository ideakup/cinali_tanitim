<div class="container">
    <div class="row">
    	<div class="col-md-5">
    		<div class="topac_back">
    			<div class="topac"></div>
    		</div>
    	</div>
    </div>
</div>
<script type="text/javascript">
    var topacanim = function(){
	  	//topac
	  	var tp1 = new TimelineMax();
	  	tp1.to($(".topac"), 0.5, {rotation:10, transformOrigin:"50% 90%", ease:Expo.easeOut});
	  	tp1.to($(".topac"), 1.5, {rotation:-20, transformOrigin:"50% 90%", ease:Expo.easeInOut});
	  	tp1.to($(".topac"), 0.5, {rotation:0, transformOrigin:"50% 90%", ease:Expo.easeIn});
	  	tp1.repeat(-1);
	  	
	  	var tp2 = new TimelineMax();
	  	tp2.to($('.topac'), 0.5, {x:'+=2', y:'+=1', transformOrigin:"50% 50%", ease:Power0.easeNone});
	  	tp2.to($('.topac'), 1.5, {x:'-=4', y:'-=2', transformOrigin:"50% 50%", ease:Power0.easeNone});
	  	tp2.to($('.topac'), 0.5, {x:'+=2', y:'+=1', transformOrigin:"50% 50%", ease:Power0.easeNone});
		tp2.repeat(-1);
	}
</script>

