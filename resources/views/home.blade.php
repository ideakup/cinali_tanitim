@php use Carbon\Carbon; @endphp

@extends('layouts.app')

@section('content')

	@include('partials.pageimages')

	@include('partials.breadcrumb')

	@if($menu->top_id == null)
	    @if ($menu->variableLang(Request::segment(1))->slug == 'cin-ali')
	    	<div class="container m-b-20">
				@include('partials.bigmenu')
			</div>
	    @endif
    @endif

    @include('partials.asidebar')

	@switch($menu->slidertype)
		@case("slider")
			<div class="container bigmenu m-b-20">
				@include('partials.slidertype')
			</div>
			@break
		@default
	@endswitch

	@if (is_null($attr))

		@if ($menu->type == 'content')

			<div class="container">
    			<div class="row">
					@if ($menu->variableLang(Request::segment(1))->title != '')
						<div class="col-md-12">
				    		<h2 class="contentHeader">{{ $menu->variableLang(Request::segment(1))->title }}</h2>
			    		</div>
		    		@endif
		    	</div>
		    </div>

			@if ($menu->variableLang(Request::segment(1))->slug == 'iletisim')
				<div class="container m-b-20">
					<div class="row">
						@include('partials.iletisim')
					</div>
				</div>
			@else

				@php $iltrFormat = 'no'; @endphp
				@if($menu->content->count() > 1)
					@if ($menu->content[0]->type == 'photo' && $menu->content[1]->type == 'text')
						@php $iltrFormat = 'yes'; @endphp
					@endif
				@endif

				@foreach ($menu->content as $element)
					
					@if ($loop->first && $element->type == 'photo' && $iltrFormat == 'yes')
						
						<div class="container m-b-20">
							<div class="row">
								<div class="col-md-4">
									<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'xlarge/'.$element->variableLang(Request::segment(1))->content) }}" alt="">
								</div>
					
					@elseif ($loop->index == 1 && $element->type == 'text' && $iltrFormat == 'yes')

								<div class="col-md-8">
									@php
										$contentArray = explode('<formore></formore>' ,$element->variableLang(Request::segment(1))->content);
										$contentPro = '';
									@endphp

									@foreach ($contentArray as $arr)
										
										@if ($loop->first && count($contentArray) > 1)
											@php
									        	$contentPro .= "";
									        @endphp
									    @endif

								   		@php
								        	$contentPro .= $arr;
								        @endphp

									    @if ($loop->first && count($contentArray) > 1)
									        @php
									        	$contentPro .= '<div class="collapse" id="collapseExample">';
									        @endphp
									    @endif

									    @if ($loop->last && count($contentArray) > 1)
									        @php
									        	$contentPro .= '</div><a class="formorebtn collapsed" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"></a>';
									        @endphp
									    @endif

									@endforeach

									{!! $contentPro !!}
								</div>

							</div>
						</div>	

					@else
						
						@if ($element->type == 'text')

							@if ($element->variableLang(Request::segment(1))->title == 'kitaptarama')

						    	<div class="container m-b-20 search_panel">
						    		<div class="row" style="padding: 0 15px;">
						    			<div class="col-md-12" style="background-color: #fcd441; background-image: url({{ url('images/patern.png') }}); background-size: 100%; background-repeat: repeat; padding: 15px;">
											<form action="http://kutuphane.cinaliyonetim.com/index.php" method="get" autocomplete="off">
												<h1>Kitap Tarama</h1>
												<p>eser adı, yazar, konu veya anahtar kelime yazarak aramaya başlayın!</p>
												<div class="s-search-div">
													<input type="text" class="s-search" id="keyword" name="keywords" value="" lang="tr_TR" aria-hidden="true" autocomplete="off" placeholder="Örn. Eğitim">
													<button type="submit" name="search" value="search" class="s-btn" formtarget="_blank"></button>
												</div>
											</form>
											<img src="{{ url('images/cinali_kutuphane.png') }}" style="margin: auto;" width="40%">
										</div>
									</div>
								</div>

							@else

								<div class="container m-b-20">
									<div class="row">
										<div class="col-md-12">

											@php
												$contentArray = explode('<formore></formore>' ,$element->variableLang(Request::segment(1))->content);
												$contentPro = '';
											@endphp

											@if(count($contentArray) == 1)

												{!! $element->variableLang(Request::segment(1))->content !!}

											@else

												@foreach ($contentArray as $arr)
													
													@if ($loop->first)
														@php
												        	$contentPro .= "";
												        @endphp
												    @endif

											   		@php
											        	$contentPro .= $arr;
											        @endphp

												    @if ($loop->first)
												        @php
												        	$contentPro .= '<div class="collapse" id="collapseExample'.$element->id.'">';
												        @endphp
												    @endif

												    @if ($loop->last)
												        @php
												        	$contentPro .= '</div><a class="formorebtn collapsed" data-toggle="collapse" href="#collapseExample'.$element->id.'" aria-expanded="false" aria-controls="collapseExample'.$element->id.'"></a>';
												        @endphp
												    @endif

												@endforeach

												{!! $contentPro !!}

											@endif

										</div>
									</div>
								</div>

							@endif 
							
						@endif

						@if ($element->type == 'photo')
							
							<div class="container m-b-20">
								<div class="row">
									<div class="col-md-12 text-center">

										@php

											$photoProps = 'class="img-responsive"';

											if($element->variableLang(Request::segment(1))->props == 'fullwidth'){
												$photoPropsCode = ' style="width:100%; height:auto;" ';
											}else if($element->variableLang(Request::segment(1))->props == 'responsive'){
												$photoPropsCode = ' class="img-responsive" ';
											}else if($element->variableLang(Request::segment(1))->props == 'fixed'){
												$photoPropsCode = '  ';
											}else{
												$photoPropsCode = ' class="img-responsive" ';
											}

										@endphp

										<img {!! $photoPropsCode !!} src="{{ url(env('APP_UPLOAD_PATH_V3').'xlarge/'.$element->variableLang(Request::segment(1))->content) }}" alt="">
										
									</div>
								</div>
							</div>
						
						@endif

						@if ($element->type == 'photogallery')
							
							<div class="container m-b-20">
								<div class="row">
									<div class="col-md-12">

										@php $i = 0; @endphp
					    				@foreach ($element->photogallery as $photo)

											@php $_colValue = 'col-md-6'; $_colCount = 2; @endphp
											@if ($element->variableLang(Request::segment(1))->props == 'col-2x')
												@php $_colValue = 'col-md-6'; $_colCount = 2; @endphp
											@elseif($element->variableLang(Request::segment(1))->props == 'col-3x')
												@php $_colValue = 'col-md-4'; $_colCount = 3; @endphp
											@elseif($element->variableLang(Request::segment(1))->props == 'col-4x')
												@php $_colValue = 'col-md-3'; $_colCount = 4; @endphp
											@endif

											@php
												$i++;
												$color01 = 'rgba(223,247,233,1)';
												$color02 = 'rgba(223,232,247,1)';
												$color03 = 'rgba(250,245,220,1)';
											@endphp
											
											@if ($i % ($_colCount * 3) == 1)
												@php $color00 = $color01; @endphp
											@elseif ($i % ($_colCount * 3) == ($_colCount + 1))
												@php $color00 = $color02; @endphp
											@elseif ($i % ($_colCount * 3) == ($_colCount + 2))
												@php $color00 = $color03; @endphp
											@endif

											@if ($i % $_colCount == 1)
						    					<div class="row" style="background-image: linear-gradient(rgba(255,0,0,0) 30%, {{ $color00 }} 30%); padding: 0 0 10px 0; margin-bottom: 20px;">
						    						<div class="container" style="padding: 0;">
														<div class="row">
									    					<div class="col-md-12">
									    	@endif

							    				<div class="{{ $_colValue }} news_list_panel2" style="padding: 15px;">
									    			<a class="group1 cboxElement" href="{{ url(env('APP_UPLOAD_PATH_V3').'xlarge/'.$photo->url) }}" title="{{ $photo->name }}" style="color:#333">
									    				<div class="col-md-12" style="padding: 0;">
															<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$photo->url) }}">
														</div>
														<div class="col-md-12" style="padding: 0;">
											    			<h5 style="color:#000000 !important;" class="eleHead">{{ $photo->name }}</h5>
														</div>
									    			</a>
										    	</div>

									    	@if ($loop->last || $i % $_colCount == 0)
									    					</div>
									    				</div>
									    			</div>
									    		</div>
									    	@endif

					    				@endforeach

									</div>
								</div>
							</div>
						
						@endif


						@if ($element->type == 'seperator')
							
							<div class="container-fluid m-b-20">
								<div class="row" style="background-color: #{{ $element->variableLang(Request::segment(1))->props }};">
		    						<div class="container">
										<div class="row">
											<div class="col-md-2"> <img class="img-responsive" src="{{ url('images/seperatorimg/sep'.rand(1,24).'.png') }}" alt=""> </div>
											<div class="col-md-8" style="display:table;">
												<p class="text-center" style="display:table-cell;vertical-align: middle;height: 160px; margin: 0; font-size: 30px;">{{ $element->variableLang(Request::segment(1))->short_content }}</p>
											</div>
											<div class="col-md-2"> <img class="img-responsive" src="{{ url('images/seperatorimg/sep'.rand(1,24).'.png') }}" alt=""> </div>
										</div>
									</div>
								</div>
							</div>
							
						@endif

						@if ($element->type == 'slide')

							<div class="container m-b-20">
								<div class="row">
									<div class="col-md-12">

										<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
									        <ol class="carousel-indicators">
									        	@foreach ($element->slide as $slide)
									                <li data-target="#carousel-example-generic" data-slide-to="{{ $loop->index }}" @if($loop->first) class="active" @endif></li>
									            @endforeach
									        </ol>
										    <div class="carousel-inner" role="listbox">
										        @foreach ($element->slide as $slide)
										            <div class="item @if ($loop->first) active @endif">
										                <img src="{{ url(env('APP_UPLOAD_PATH_V3').'xlarge/'.$slide->image_url) }}" style="width: 100%;">
										                <div class="carousel-caption"></div>
										            </div>
										        @endforeach
										    </div>
										    <!-- Controls -->
										    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
										        <!--<button type="button" class="slider-prev"><span aria-label="Önceki">›</span></button>-->
										        <span class="slider-prev" aria-hidden="true"></span>
										        <span class="sr-only">Önceki</span>
										    </a>
										    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
										        <!--<button type="button" class="slider-next"><span aria-label="Sonraki">›</span></button>-->
										        <span class="slider-next" aria-hidden="true"></span>
										    <span class="sr-only">Sonraki</span>
										    </a>
										</div>

									</div>
								</div>
							</div>

						@endif

						@if ($element->type == 'form')

							<div class="container m-b-20">
								<div class="row">

									<script type="text/javascript">
										var formData_{{ $element->id }} = '{!! $element->variableLang(Request::segment(1))->content !!}';
									</script>

									<div class="col-xs-12 col-md-8 col-md-offset-2">

										<form id="form_{{ $element->id }}" method="POST" action="{{ url('tr/form_save') }}">
											{{ csrf_field() }}
		        							<input type="hidden" name="lang" value="{{ Request::segment(1) }}">
		        							<input type="hidden" name="source_type" value="menu">
											<input type="hidden" name="source_id" value="{{ $menu->id }}">
											<input type="hidden" name="form_id" value="{{ $element->id }}">
											<div id="form_element_{{ $element->id }}"></div>
											
											<button type="submit" id="form_submit_{{ $element->id }}" class="btn ladda-button" data-color="first" data-style="expand-right" data-size="s" style="outline: 0;"><span class="ladda-label"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{ json_decode($element->variableLang(Request::segment(1))->props)->props_buttonname }} </span><span class="ladda-spinner"></span></button>
										</form>

									</div>

								</div>
							</div>

						@endif

						@if ($element->type == 'file')
							
							<div class="container m-b-20">
								<div class="row">
									<div class="col-md-12 text-center">
										<a href="{{ url(env('APP_UPLOAD_PATH_V3').'files/'.$element->variableLang(Request::segment(1))->content) }}" target="_blank"> <h3>{{ $element->variableLang(Request::segment(1))->title }}</h3></a>
									</div>
								</div>
							</div>

						@endif

					@endif
						
    			@endforeach
					
			@endif

		@endif

		@if ($menu->type == 'list')
			
			<div class="container">
    			<div class="row">
					
					<div class="col-md-5">
			    		@if ($menu->variableLang(Request::segment(1))->title != '')<h2 class="galeryHeader">{{ $menu->variableLang(Request::segment(1))->menutitle }}</h2>@endif
		    		</div>

		    		@php
						$neredeFilterPostArr = array();
						if(isset($_POST['neredeFilter'])){
							$neredeFilterPostArr = $_POST['neredeFilter'];
							if($neredeFilterPostArr[0]==""){
								array_shift($neredeFilterPostArr);
							}
							if($neredeFilterPostArr[0]=="all"){
								array_shift($neredeFilterPostArr);
							}
						}

						$neFilterPostArr = array();
						if(isset($_POST['neFilter'])){
							$neFilterPostArr = $_POST['neFilter'];
							if($neFilterPostArr[0]==""){
								array_shift($neFilterPostArr);
							}
							if($neFilterPostArr[0]=="all"){
								array_shift($neFilterPostArr);
							}
						}

						$nezamanFilterPostArr = array();
						if(isset($_POST['nezamanFilter'])){
							$nezamanFilterPostArr = $_POST['nezamanFilter'];
							if($nezamanFilterPostArr[0]==""){
								array_shift($nezamanFilterPostArr);
							}
							if($nezamanFilterPostArr[0]=="all"){
								array_shift($nezamanFilterPostArr);
							}
						}
					@endphp
		    		
		    		<form method="POST" action="{{ url()->current() }}" id="filterForm" style="padding: 15px 15px;">
		                {{ csrf_field() }}
		                <input type="hidden" name="crud" value="{{ Request::segment(2) }}">
		                <input type="hidden" name="menu_id" value="{{ Request::segment(3) }}">
		                <input type="hidden" name="lang" value="{{ Request::segment(4) }}">

						<div class="col-md-1">
							<button type="button" id="resetButton" class="btn ladda-button" style="float: right; font-size: 18px; background-color: #DAF6E6; border-radius: 12px; color: #999 !important; border: 2px solid #aaa; padding: 6px 12px;"><i class="fa fa-trash" aria-hidden="true"></i></button>
						</div>

						<div class="col-md-2" id="neredeFilterDiv">
						    <select class="form-control m-select2" id="neredeFilter" name="neredeFilter[]" multiple="multiple">
						    	<option value="all" @if(count($neredeFilterPostArr) == 0) selected="" @endif>Tümü</option>
						    	@foreach ($categoriesAvailable as $ccont)

						    		@php $selected = ""; @endphp
						    		@foreach ($neredeFilterPostArr as $neredeFPA)
						    			@if($neredeFPA == $ccont->id)
						    				@php $selected = 'selected=""'; @endphp
						    			@endif
						    		@endforeach
									
									<option value="{{ $ccont->id }}" {{ $selected }}> {{ $ccont->variableLang(Request::segment(1))->title }} </option>
									
								@endforeach
						    </select>
						</div>

						<div class="col-md-2" id="neFilterDiv">
	                        <select class="form-control m-select2" id="neFilter" name="neFilter[]" multiple="multiple">
	                        	<option value="all" @if(count($neFilterPostArr) == 0) selected="" @endif>Tümü</option>
	                        	@foreach ($tagsAvailable as $tcont)

	                        		@php $selected = ""; @endphp
	                        		@foreach ($neFilterPostArr as $neFPA)
	                        			@if($neFPA == $tcont->id)
	                        				@php $selected = 'selected=""'; @endphp
	                        			@endif
	                        		@endforeach
									
									<option value="{{ $tcont->id }}" {{ $selected }}> {{ $tcont->variableLang(Request::segment(1))->title }} </option>

								@endforeach
	                        </select>
						</div>

						<div class="col-md-2" id="nezamanFilterDiv" style="padding-right: 0;">
	                        <select class="form-control m-select2" id="nezamanFilter" name="nezamanFilter[]" multiple="multiple">
	                        	<option value="all" @if(count($nezamanFilterPostArr) == 0) selected="" @endif>Tümü</option>
		                        @php
		                        	$AyArray = array('Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık');
		                        	$nowDate = Carbon::now();
		                        	$maxDate = Carbon::parse($menu->content->max('release_date'));
		                        	$minDate = Carbon::parse($menu->content->min('release_date'));
								@endphp

								@if($nowDate->year == $maxDate->year)
									@for ($i = $maxDate->month; $i > 0; $i--)

										@php $selected = ""; @endphp
		                        		@foreach ($nezamanFilterPostArr as $nezamanFPA)
		                        			@if($nezamanFPA == $i.'-'.$maxDate->year)
		                        				@php $selected = 'selected=""'; @endphp
		                        			@endif
		                        		@endforeach

										<option value="{{$i}}-{{ $maxDate->year }}" {{ $selected }}> {{ $AyArray[$i-1] }} {{ $maxDate->year }} </option>

									@endfor
								@endif

								@if($nowDate->year > $minDate->year)
									@for ($i = ($maxDate->year-1); $i >= $minDate->year; $i--)

										@php $selected = ""; @endphp
		                        		@foreach ($nezamanFilterPostArr as $nezamanFPA)
		                        			@if($nezamanFPA == $i)
		                        				@php $selected = 'selected=""'; @endphp
		                        			@endif
		                        		@endforeach

										<option value="{{ $i }}" {{ $selected }}> {{ $i }} Yılı </option>

									@endfor
								@endif
							</select>
                        </div>

					</form>

					@php
						$aaa = $menu->content;
					@endphp
					@foreach ($aaa as $key => $element)
						@php

							$neredeFilterPulled = 0;
							if(isset($neredeFilterPostArr) && count($neredeFilterPostArr) > 0){
								$neredeFilterPulled = 1;
								if($element->categories->whereIn('id', $neredeFilterPostArr)->count() > 0){
									$neredeFilterPulled = 2;
								}
							}
							
							$neFilterPulled = 0;
							if(isset($neFilterPostArr) && count($neFilterPostArr) > 0){
								$neFilterPulled = 1;
								if($element->tags->whereIn('id', $neFilterPostArr)->count() > 0){
									$neFilterPulled = 2;
								}
							}

							$nezamanFilterPulled = 0;
							if(isset($nezamanFilterPostArr) && count($nezamanFilterPostArr) > 0){
								$nezamanFilterPulled = 1;

								$sssss1 = Carbon::parse($element->release_date)->month.'-'.Carbon::parse($element->release_date)->year;
								$sssss2 = Carbon::parse($element->release_date)->year.'';

								if(in_array($sssss1, $nezamanFilterPostArr) || in_array($sssss2, $nezamanFilterPostArr)){
									$nezamanFilterPulled = 2;
								}
							}

							if($neredeFilterPulled == 2 || $neFilterPulled == 2 || $nezamanFilterPulled == 2){
							}else if($neredeFilterPulled == 0 && $neFilterPulled == 0 && $nezamanFilterPulled == 0){
							}else{
								$aaa->pull($key);
							}
						@endphp
					@endforeach
					
				</div>
		    </div>
			
			<div class="container-fluid">
				
				@php $i = 0; @endphp
				@foreach ($menu->content as $element)

					@php $isVisible = true; @endphp

					@if ($isVisible)

						@php $_colValue = 'col-md-6'; $_colCount = 2;@endphp
						@if ($menu->listtype == 'col-2x')
							@php $_colValue = 'col-md-6'; $_colCount = 2; @endphp
						@elseif($menu->listtype == 'col-3x')
							@php $_colValue = 'col-md-4'; $_colCount = 3; @endphp
						@elseif($menu->listtype == 'col-4x')
							@php $_colValue = 'col-md-3'; $_colCount = 4; @endphp
						@endif

						@php
							$i++;
							$color01 = 'rgba(223,247,233,1)';
							$color02 = 'rgba(223,232,247,1)';
							$color03 = 'rgba(250,245,220,1)';
						@endphp

						@if ($i % ($_colCount * 3) == 1)
							@php $color00 = $color01; @endphp
						@elseif ($i % ($_colCount * 3) == ($_colCount + 1))
							@php $color00 = $color02; @endphp
						@elseif ($i % ($_colCount * 3) == ($_colCount + 2))
							@php $color00 = $color03; @endphp
						@endif

						@if ($element->type == 'group')

							@if ($i % $_colCount == 1)
					    		<div class="row" style="background-image: linear-gradient(rgba(255,0,0,0) 30%, {{ $color00 }} 30%); padding: 0 0 10px 0; margin-bottom: 20px;">
		    						<div class="container" style="padding: 0;">
										<div class="row">
					    					<div class="col-md-12">
					    	@endif

							<div class="{{ $_colValue }} news_list_panel2" style="padding: 15px;">

								@php
				                    $dDetectMenu = $menu;
				                    while ($dDetectMenu->topMenu != null) {
				                        $dDetectMenu = $dDetectMenu->topMenu;
				                    }
				                    if(is_null(config('domains.'.$dDetectMenu->variableLang($lang)->slug))){
				                        $domainMenu = config('domains.cin-ali');
				                    }else{
				                        $domainMenu = config('domains.'.$dDetectMenu->variableLang($lang)->slug);
				                    }
				                @endphp

				    			<a href="{{ $domainMenu.'/'.$lang.'/'.$menu->variableLang(Request::segment(1))->slug.'/'.$element->variableLang(Request::segment(1))->slug }}">
				    				<div class="col-md-12" style="padding: 0;">
										@if ($element->sub_content->count() == 0)
											<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'default_news.jpg') }}">
										@else
											@if (!is_null($element->sub_content->where('type', 'photo')->first()))
												<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$element->sub_content->where('type', 'photo')->first()->variableLang(Request::segment(1))->content) }}">
											@elseif (!is_null($element->sub_content->where('type', 'photogallery')->first()))
												<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$element->sub_content->where('type', 'photogallery')->first()->photogallery->first()->url) }}">
											@else
												<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'default_news.jpg') }}">
											@endif
										@endif
									</div>
									<div class="col-md-12" style="padding: 0;">
						    			<h4 class="eleHead" style="margin-bottom: 10px;">{{ $element->variableLang(Request::segment(1))->title }}</h4>
						    			<p style="font-size:14px" class="eleText">
						    				{!! $element->variableLang(Request::segment(1))->short_content !!}
										</p>
									</div>
				    			</a>

					    	</div>

					    	@if ($loop->last || $i % $_colCount == 0)
					    					</div>
					    				</div>
					    			</div>
					    		</div>
					    	@endif

						@endif

					@endif

				@endforeach

			</div>

		@endif

		@if ($menu->type == 'photogallery')

			<div class="container">
    			<div class="row">
					
					<div class="col-md-5">
			    		<h2 class="galeryHeader">@if ($menu->variableLang(Request::segment(1))->title != ''){{ $menu->variableLang(Request::segment(1))->menutitle }}@endif</h2>
		    		</div>

					@php
						$neredeFilterPostArr = array();
						if(isset($_POST['neredeFilter'])){
							$neredeFilterPostArr = $_POST['neredeFilter'];
							if($neredeFilterPostArr[0]==""){
								array_shift($neredeFilterPostArr);
							}
							if($neredeFilterPostArr[0]=="all"){
								array_shift($neredeFilterPostArr);
							}
						}

						$neFilterPostArr = array();
						if(isset($_POST['neFilter'])){
							$neFilterPostArr = $_POST['neFilter'];
							if($neFilterPostArr[0]==""){
								array_shift($neFilterPostArr);
							}
							if($neFilterPostArr[0]=="all"){
								array_shift($neFilterPostArr);
							}
						}

						$nezamanFilterPostArr = array();
						if(isset($_POST['nezamanFilter'])){
							$nezamanFilterPostArr = $_POST['nezamanFilter'];
							if($nezamanFilterPostArr[0]==""){
								array_shift($nezamanFilterPostArr);
							}
							if($nezamanFilterPostArr[0]=="all"){
								array_shift($nezamanFilterPostArr);
							}
						}

					@endphp

		    		<form method="POST" action="{{ url()->current() }}" id="filterForm" style="padding: 15px 15px;">
		                {{ csrf_field() }}
		                <input type="hidden" name="crud" value="{{ Request::segment(2) }}">
		                <input type="hidden" name="menu_id" value="{{ Request::segment(3) }}">
		                <input type="hidden" name="lang" value="{{ Request::segment(4) }}">

						<div class="col-md-1">
							<button type="button" id="resetButton" class="btn ladda-button" style="float: right; font-size: 18px; background-color: #DAF6E6; border-radius: 12px; color: #999 !important; border: 2px solid #aaa; padding: 6px 12px;"><i class="fa fa-trash" aria-hidden="true"></i></button>
						</div>

						<div class="col-md-2" id="neredeFilterDiv">
						    <select class="form-control m-select2" id="neredeFilter" name="neredeFilter[]" multiple="multiple">
						    	<option value="all" @if(count($neredeFilterPostArr) == 0) selected="" @endif>Tümü</option>
						    	@foreach ($categoriesAvailable as $ccont)

						    		@php $selected = ""; @endphp
						    		@foreach ($neredeFilterPostArr as $neredeFPA)
						    			@if($neredeFPA == $ccont->id)
						    				@php $selected = 'selected=""'; @endphp
						    			@endif
						    		@endforeach
									
									<option value="{{ $ccont->id }}" {{ $selected }}> {{ $ccont->variableLang(Request::segment(1))->title }} </option>
									
								@endforeach
						    </select>
						</div>

						<div class="col-md-2" id="neFilterDiv">
	                        <select class="form-control m-select2" id="neFilter" name="neFilter[]" multiple="multiple">
	                        	<option value="all" @if(count($neFilterPostArr) == 0) selected="" @endif>Tümü</option>
	                        	@foreach ($tagsAvailable as $tcont)

	                        		@php $selected = ""; @endphp
	                        		@foreach ($neFilterPostArr as $neFPA)
	                        			@if($neFPA == $tcont->id)
	                        				@php $selected = 'selected=""'; @endphp
	                        			@endif
	                        		@endforeach
									
									<option value="{{ $tcont->id }}" {{ $selected }}> {{ $tcont->variableLang(Request::segment(1))->title }} </option>

								@endforeach
	                        </select>
						</div>

						<div class="col-md-2" id="nezamanFilterDiv" style="padding-right: 0;">
	                        <select class="form-control m-select2" id="nezamanFilter" name="nezamanFilter[]" multiple="multiple">
	                        	<option value="all" @if(count($nezamanFilterPostArr) == 0) selected="" @endif>Tümü</option>
		                        @php
		                        	$AyArray = array('Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık');
		                        	$nowDate = Carbon::now();
		                        	$maxDate = Carbon::parse($menu->content->max('release_date'));
		                        	$minDate = Carbon::parse($menu->content->min('release_date'));
								@endphp

								@if($nowDate->year == $maxDate->year)
									@for ($i = $maxDate->month; $i > 0; $i--)

										@php $selected = ""; @endphp
		                        		@foreach ($nezamanFilterPostArr as $nezamanFPA)
		                        			@if($nezamanFPA == $i.'-'.$maxDate->year)
		                        				@php $selected = 'selected=""'; @endphp
		                        			@endif
		                        		@endforeach

										<option value="{{$i}}-{{ $maxDate->year }}" {{ $selected }}> {{ $AyArray[$i-1] }} {{ $maxDate->year }} </option>

									@endfor
								@endif

								@if($nowDate->year > $minDate->year)
									@for ($i = ($maxDate->year-1); $i >= $minDate->year; $i--)

										@php $selected = ""; @endphp
		                        		@foreach ($nezamanFilterPostArr as $nezamanFPA)
		                        			@if($nezamanFPA == $i)
		                        				@php $selected = 'selected=""'; @endphp
		                        			@endif
		                        		@endforeach

										<option value="{{ $i }}" {{ $selected }}> {{ $i }} Yılı </option>

									@endfor
								@endif
							</select>
                        </div>

					</form>

					@php
						$aaa = $menu->content;
					@endphp
					@foreach ($aaa as $key => $element)
						@php

							$neredeFilterPulled = 0;
							if(isset($neredeFilterPostArr) && count($neredeFilterPostArr) > 0){
								$neredeFilterPulled = 1;
								if($element->categories->whereIn('id', $neredeFilterPostArr)->count() > 0){
									$neredeFilterPulled = 2;
								}
							}
							
							$neFilterPulled = 0;
							if(isset($neFilterPostArr) && count($neFilterPostArr) > 0){
								$neFilterPulled = 1;
								if($element->tags->whereIn('id', $neFilterPostArr)->count() > 0){
									$neFilterPulled = 2;
								}
							}

							$nezamanFilterPulled = 0;
							if(isset($nezamanFilterPostArr) && count($nezamanFilterPostArr) > 0){
								$nezamanFilterPulled = 1;

								$sssss1 = Carbon::parse($element->release_date)->month.'-'.Carbon::parse($element->release_date)->year;
								$sssss2 = Carbon::parse($element->release_date)->year.'';

								if(in_array($sssss1, $nezamanFilterPostArr) || in_array($sssss2, $nezamanFilterPostArr)){
									$nezamanFilterPulled = 2;
								}
							}

							if($neredeFilterPulled == 2 || $neFilterPulled == 2 || $nezamanFilterPulled == 2){
							}else if($neredeFilterPulled == 0 && $neFilterPulled == 0 && $nezamanFilterPulled == 0){
							}else{
								$aaa->pull($key);
							}
						@endphp
					@endforeach
						
	    		</div>
		    </div>
			
			<div class="container-fluid">
				
				@php $i = 0; @endphp
				@foreach ($menu->content as $element)

					@php $isVisible = true; @endphp

					@if ($isVisible)

						@php $_colValue = 'col-md-6'; $_colCount = 2; @endphp
						@if ($menu->listtype == 'col-2x')
							@php $_colValue = 'col-md-6'; $_colCount = 2; @endphp
						@elseif($menu->listtype == 'col-3x')
							@php $_colValue = 'col-md-4'; $_colCount = 3; @endphp
						@elseif($menu->listtype == 'col-4x')
							@php $_colValue = 'col-md-3'; $_colCount = 4; @endphp
						@endif

						@php
							$i++;
							$color01 = 'rgba(223,247,233,1)';
							$color02 = 'rgba(223,232,247,1)';
							$color03 = 'rgba(250,245,220,1)';
						@endphp
						
						@if ($i % ($_colCount * 3) == 1)
							@php $color00 = $color01; @endphp
						@elseif ($i % ($_colCount * 3) == ($_colCount + 1))
							@php $color00 = $color02; @endphp
						@elseif ($i % ($_colCount * 3) == ($_colCount + 2))
							@php $color00 = $color03; @endphp
						@endif

						@if ($element->type == 'photogallery')
							
							@if ($i % $_colCount == 1)
								<div class="row" style="background-image: linear-gradient(rgba(255,0,0,0) 30%, {{ $color00 }} 30%); padding: 0 0 10px 0; margin-bottom: 20px;">
			    						<div class="container" style="padding: 0;">
											<div class="row">
						    					<div class="col-md-12">
					    	@endif

					    	<div class="{{ $_colValue }} news_list_panel2" style="padding: 15px;">
								
								@php
				                    $dDetectMenu = $menu;
				                    while ($dDetectMenu->topMenu != null) {
				                        $dDetectMenu = $dDetectMenu->topMenu;
				                    }
				                    if(is_null(config('domains.'.$dDetectMenu->variableLang($lang)->slug))){
				                        $domainMenu = config('domains.cin-ali');
				                    }else{
				                        $domainMenu = config('domains.'.$dDetectMenu->variableLang($lang)->slug);
				                    }
				                @endphp

				    			<a href="{{ $domainMenu.'/'.$lang.'/'.$menu->variableLang(Request::segment(1))->slug.'/'.$element->variableLang(Request::segment(1))->slug }}">
				    				<div class="col-md-12" style="padding: 0;">

				    					@if ($element->photogallery->count() == 0)
				    						
				    						<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'default_news.jpg') }}">

				    					@else

				    						<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$element->photogallery->first()->url) }}">

				    					@endif

									</div>
									<div class="col-md-12">
						    			<h4 class="eleHead">{{ $element->variableLang(Request::segment(1))->title }}</h4>
						    			@if(!is_null($element->variableLang(Request::segment(1))->short_content))
						    				<h5>{{ $element->variableLang(Request::segment(1))->short_content }}</h5>
						    			@endif
									</div>
				    			</a>
				    			
					    	</div>

					    	@if ($loop->last || $i % $_colCount == 0)
					    					</div>
					    				</div>
					    			</div>
					    		</div>
					    	@endif
						@endif

						@if (false && $element->type == 'text')
							<div class="{{ $_colValue }} news_list_panel2" style="padding: 15px;">
							
								@php
				                    $dDetectMenu = $menu;
				                    while ($dDetectMenu->topMenu != null) {
				                        $dDetectMenu = $dDetectMenu->topMenu;
				                    }
				                    if(is_null(config('domains.'.$dDetectMenu->variableLang($lang)->slug))){
				                        $domainMenu = config('domains.cin-ali');
				                    }else{
				                        $domainMenu = config('domains.'.$dDetectMenu->variableLang($lang)->slug);
				                    }
				                @endphp

				    			<a href="{{ $domainMenu.'/'.$lang.'/'.$menu->variableLang(Request::segment(1))->slug.'/'.$element->id }}">
									<div class="col-md-12" style="padding: 0;">

										@if ($element->sub_content->count() == 0)
											<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'default_news.jpg') }}">
										@else
											@if (!is_null($element->sub_content->where('type', 'photo')->first()))
												<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail169/'.$element->sub_content->where('type', 'photo')->first()->variableLang(Request::segment(1))->content) }}">
											@elseif (!is_null($element->sub_content->where('type', 'photogallery')->first()))
												<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail169/'.$element->sub_content->where('type', 'photogallery')->first()->photogallery->first()->url) }}">
											@else
												<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'default_news.jpg') }}">
											@endif
										@endif
										
									</div>
									<div class="col-md-12" style="padding: 0;">
						    			<h4 style="font-size:17px; color:#000000 !important;" class="eleHead">{{ $element->variableLang(Request::segment(1))->title }}</h4>
						    			<p style="font-size:13px" class="eleText">
						    				{!! str_limit(strip_tags($element->variableLang(Request::segment(1))->content), 140) !!}
										</p>
									</div>
									<div class="col-md-6" style="padding: 0; font-size:12px;">
										@if ($element->tags->count() > 0)
											{{ $element->tags->first()->variableLang(Request::segment(1))->title }}
										@endif
									</div>	
									<div class="col-md-6" style="padding: 0; font-size:12px; text-align: right;">{{-- Carbon::parse($element->created_at)->format('d.m.Y') --}}</div>
								</a>
							</div>
						@endif

					@endif

				@endforeach
			</div>

		@endif

		@if ($menu->type == 'calendar')

			<div class="container m-b-20 content">
    			<div class="row">

	                <div class="col-md-12">
						<div id='calendarScript'></div>
		                <p> </p>
		            </div>

					@if ($menu->variableLang(Request::segment(1))->title != '')
						<div class="col-md-12">
				    		<h2 class="contentHeader">{{ $menu->variableLang(Request::segment(1))->title }}</h2>
			    		</div>
		    		@endif

		    		@foreach ($menu->content as $element)

						@if ($element->type == 'photogallery')

							<div class="row">
								@php $_colValue = 'col-md-6';@endphp
								@if ($element->variableLang(Request::segment(1))->props == 'col-2x')
									@php $_colValue = 'col-md-6';@endphp
								@elseif($element->variableLang(Request::segment(1))->props == 'col-3x')
									@php $_colValue = 'col-md-4';@endphp
								@elseif($element->variableLang(Request::segment(1))->props == 'col-4x')
									@php $_colValue = 'col-md-3';@endphp
								@endif

			    				@foreach ($element->photogallery as $photo)
				    				<div class="{{ $_colValue }} news_list_panel2" style="padding: 15px;">
						    			<a class="group1 cboxElement" href="{{ url(env('APP_UPLOAD_PATH_V3').'large/'.$photo->url) }}" title="{{ $photo->name }}" style="color:#333">
						    				<div class="col-md-12" style="padding: 0;">
												<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail2x/'.$photo->url) }}">
											</div>
											<div class="col-md-12" style="padding: 0;">
								    			<h5 style="color:#000000 !important;" class="eleHead">{{ $photo->name }}</h5>
											</div>
						    			</a>
							    	</div>
			    				@endforeach
							</div>
						@endif

						@if ($element->type == 'text')
							<div class="col-md-12">
								{!! $element->variableLang(Request::segment(1))->content !!}
							</div>
						@endif

						@if ($element->type == 'photo')
							<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'xlarge/'.$element->variableLang(Request::segment(1))->content) }}" alt="" style="margin-bottom: 10px;">
						@endif

						@if ($element->type == 'form')

							<script type="text/javascript">
								var formData_{{ $element->id }} = '{!! $element->variableLang(Request::segment(1))->content !!}';
							</script>
							<div class="col-xs-12 col-md-8 col-md-offset-2">
								<form id="form_{{ $element->id }}" method="POST" action="{{ url('tr/form_save') }}">
									{{ csrf_field() }}
        							<input type="hidden" name="lang" value="{{ Request::segment(1) }}">
        							<input type="hidden" name="source_type" value="menu">
									<input type="hidden" name="source_id" value="{{ $menu->id }}">
									<input type="hidden" name="form_id" value="{{ $element->id }}">
									<div id="form_element_{{ $element->id }}"></div>

									<button type="submit" id="form_submit_{{ $element->id }}" class="btn ladda-button" data-color="first" data-style="expand-right" data-size="s" style="outline: 0;"><span class="ladda-label"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{ json_decode($element->variableLang(Request::segment(1))->props)->props_buttonname }} </span><span class="ladda-spinner"></span></button>
								</form>
							</div>

						@endif

	    			@endforeach

				</div>
			</div>

		@endif

	@else

		@if ($menu->type == 'calendar') 

			<div class="container">
				<div class="row">
					<!-- TAKVİM DETAY -->
					@php
						$event = $calendar->find($attr);
	    				//Session::flash('status', 'Task was successful!');
	    				Session::put('d', ''.Carbon::parse($event->start)->format("Y-m-d"));
	    				//dump(Session::get('d'));
					@endphp

					@if ($menu->variableLang(Request::segment(1))->title != '')
						<div class="col-md-12" style="background-color: #fdd629;">
							@php
								$ay = Carbon::parse($event->start)->format("m");
								$ayMetin = '';
								if ($ay == 1) {
									$ayMetin = 'Ocak';
								}else if ($ay == 2) {
									$ayMetin = 'Şubat';
								}else if ($ay == 3) {
									$ayMetin = 'Mart';
								}else if ($ay == 4) {
									$ayMetin = 'Nisan';
								}else if ($ay == 5) {
									$ayMetin = 'Mayıs';
								}else if ($ay == 6) {
									$ayMetin = 'Haziran';
								}else if ($ay == 7) {
									$ayMetin = 'Temmuz';
								}else if ($ay == 8) {
									$ayMetin = 'Ağustos';
								}else if ($ay == 9) {
									$ayMetin = 'Eylül';
								}else if ($ay == 10) {
									$ayMetin = 'Ekim';
								}else if ($ay == 11) {
									$ayMetin = 'Kasım';
								}else if ($ay == 12) {
									$ayMetin = 'Aralık';
								} 
							@endphp
				    		<h2 class="eventTopHeader">{{ $menu->variableLang(Request::segment(1))->title }}</h2><h2 class="eventTopHeaderDate">{{ $ayMetin }} {{ Carbon::parse($event->start)->format("Y") }}</h2>
			    		</div>
					@endif
					
					<div class="col-md-12">
						<style type="text/css">
							.contentHeader > a {
								color:#000;
							}
						</style>
			    		<h2 class="contentHeader">
		    				@php
			                    $dDetectMenu = $menu;
			                    while ($dDetectMenu->topMenu != null) {
			                        $dDetectMenu = $dDetectMenu->topMenu;
			                    }
			                    if(is_null(config('domains.'.$dDetectMenu->variableLang($lang)->slug))){
			                        $domainMenu = config('domains.cin-ali');
			                    }else{
			                        $domainMenu = config('domains.'.$dDetectMenu->variableLang($lang)->slug);
			                    }
			                @endphp
			    			<a href="{{ $domainMenu.'/'.Request::segment(1).'/etkinlikler' }}"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
			    		</h2>
					</div>

					@if (!is_null($event->variableLang(Request::segment(1))->image_name))
						<div class="col-md-4">
							<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'/xlarge/'.$event->variableLang(Request::segment(1))->image_name) }}">
						</div>
						@php $contentDivCol = 8; @endphp
					@else
						@php $contentDivCol = 12; @endphp
					@endif

					<div class="col-md-{{ $contentDivCol }}">
						
						@if (is_null($event->price) || $event->price == 0) 
							<span style="position: absolute; right: 0; top: -60px; background-color: #fdd629; border-radius: 50%; width: 100px; height: 100px; font-size: 20px; text-align: center; padding: 23px 0;">
								Ücretsiz Etkinlik 
							</span>
						@endif
						
						@if ($event->variableLang(Request::segment(1))->title != '')
					    	<h2 class="eventHeader" @if (is_null($event->price) || $event->price == 0) style="width: 85%;" @endif>{{ $event->variableLang(Request::segment(1))->title }}</h2>
			    		@endif
						
						{!! $event->variableLang(Request::segment(1))->content !!}
						
						<style type="text/css">
							.eventDetail p{
								font-size: 18px;
								margin-bottom: 5px;
							}
							.eventDetail p .spp{
								display: inline-block;
								width: 80px;
							}
						</style>

						<div class="eventDetail">
							
							@php

								$startDate = Carbon::parse($event->start)->format("d M Y - l");
								$searchDay  = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
								$replaceDay = array('Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi', 'Pazar');
								$startDate2 = str_replace($searchDay, $replaceDay, $startDate);

								$searchMonth  = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
								$replaceMonth = array('Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık');
								$startDate3 = str_replace($searchMonth, $replaceMonth, $startDate2);

							@endphp

							<p><span class="spp">Tarih</span>: {{ $startDate3 }}</p>
							<p><span class="spp">Saat</span>: {{ Carbon::parse($event->start)->format("H:i") }}</p>


							@if (!is_null($event->end))
								@php

									$endDate = Carbon::parse($event->end)->format("d M Y - l");
									$endDate2 = str_replace($searchDay, $replaceDay, $endDate);
									$endDate3 = str_replace($searchMonth, $replaceMonth, $endDate2);

								@endphp
								<p><span class="spp">Bitiş Tarihi</span>: {{ $endDate3 }}</p>
								<p><span class="spp">Bitiş Saati</span>: {{ Carbon::parse($event->end)->format("H:i") }}</p>
								
							@endif

							<p><span class="spp">Kapasite</span>:
								@if ($event->capacity == -1)
									Kapalı Grup
								@elseif ($event->capacity == 0)
									Sınırsız
								@else
									{{ $event->capacity }} Kişi
								@endif
							</p>
							<p>
								@if ($event->price != 0)
									<span class="spp">Ücret</span>: {{ $event->price }} ₺
								@endif
							</p>
							{!! $event->variableLang(Request::segment(1))->address !!}
						</div>

						@if(Carbon::parse($event->start)->greaterThan(Carbon::now('Europe/Istanbul')))

							@if (!is_null($event->reservationForm))
								@if ( ($event->capacity > 0 && $event->reservationFormData->count() < $event->capacity) || $event->capacity == 0 || $event->capacity == -1)
									<script type="text/javascript">
										var formData_{{ $event->id }} = '{!! $event->reservationForm->variableLang(Request::segment(1))->content !!}';
									</script>
									<div class="col-md-6" style="padding: 0; padding-bottom: 15px;">
										<form id="form_{{ $event->reservationForm->id }}" method="POST" action="{{ url('tr/form_save') }}">
											{{ csrf_field() }}
											<input type="hidden" name="lang" value="{{ Request::segment(1) }}">
											<input type="hidden" name="source_type" value="calendar">
											<input type="hidden" name="source_id" value="{{ $event->id }}">
											<input type="hidden" name="form_id" value="{{ $event->reservationForm->id }}">
											<div id="form_element_{{ $event->reservationForm->id }}"></div>
											<button type="submit" id="form_submit_{{ $event->reservationForm->id }}" class="btn ladda-button" data-color="first" data-style="expand-right" data-size="s" style="outline: 0;"><span class="ladda-label"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{ json_decode($event->reservationForm->variableLang(Request::segment(1))->props)->props_buttonname }} </span><span class="ladda-spinner"></span></button>
										</form>
									</div>
								@else
									<div class="col-md-12" style="padding: 0;">
										{!! $event->variableLang(Request::segment(1))->nocapmessage !!}
									</div>
								@endif


							@endif

						@endif
					
					</div>
				</div>
			</div>
		@else

			@if ($content->type == 'text')
				
				<div class="container">
					<div class="row">
						@if ($content->variableLang(Request::segment(1))->title != '')
							<div class="col-md-12">
					    		<h2 class="contentHeader">{{ $content->variableLang(Request::segment(1))->title }}</h2>
				    		</div>
			    		@endif
						
						@php $iltrFormat = 'no'; @endphp

						@if($content->sub_content->count() > 0)
							@if ($content->sub_content[0]->type == 'photo')
								@php $iltrFormat = 'yes'; @endphp
							@endif
						@endif
						
						@foreach ($content->sub_content as $element)

							@if ($loop->first && $element->type == 'photo' && $iltrFormat == 'yes')

								<div class="col-md-4">
									<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'xlarge/'.$element->variableLang(Request::segment(1))->content) }}" alt="">
								</div>

								<div class="col-md-8">
									{!! $content->variableLang(Request::segment(1))->content !!}
								</div>

							@elseif($element->type == 'photogallery')
								
								<div class="col-md-12">
									@php $_colValue = 'col-md-6';@endphp
									@if ($element->variableLang(Request::segment(1))->props == 'col-2x')
										@php $_colValue = 'col-md-6';@endphp
									@elseif($element->variableLang(Request::segment(1))->props == 'col-3x')
										@php $_colValue = 'col-md-4';@endphp
									@elseif($element->variableLang(Request::segment(1))->props == 'col-4x')
										@php $_colValue = 'col-md-3';@endphp
									@endif

				    				@foreach ($element->photogallery as $photo)
				    					
			    						<div class="{{ $_colValue }} news_list_panel2" style="padding: 15px;">
							    			<a class="group1 cboxElement" href="{{ url(env('APP_UPLOAD_PATH_V3').'large/'.$photo->url) }}" title="{{ $photo->name }}" style="color:#333">
							    				<div class="col-md-12" style="padding: 0;">
													<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$photo->url) }}">
												</div>
												<div class="col-md-12" style="padding: 0;">
									    			<h5 style="color:#000000 !important;" class="eleHead">{{ $photo->name }}</h5>
												</div>
							    			</a>
								    	</div>
								    	
				    				@endforeach

				    			</div>

							@endif
						@endforeach

					</div>
				</div>
				
			@endif
			
			@if ($content->type == 'photogallery')
				
				<div class="container">
					<div class="row">
						<div class="col-md-12">
			    			<h2 class="galeryHeader">{{ $content->variableLang(Request::segment(1))->title }}</h2>

			    			@if(!is_null($content->variableLang(Request::segment(1))->short_content))
			    				<p>{{ $content->variableLang(Request::segment(1))->short_content }}</p>
			    			@endif
			    		</div>
			    	</div>
			    </div>

			    <div class="container-fluid">
					
					@php $i = 0; @endphp
    				@foreach ($content->photogallery as $photo)

						@php $_colValue = 'col-md-6'; $_colCount = 2; @endphp
						@if ($content->variableLang(Request::segment(1))->props == 'col-2x')
							@php $_colValue = 'col-md-6'; $_colCount = 2; @endphp
						@elseif($content->variableLang(Request::segment(1))->props == 'col-3x')
							@php $_colValue = 'col-md-4'; $_colCount = 3; @endphp
						@elseif($content->variableLang(Request::segment(1))->props == 'col-4x')
							@php $_colValue = 'col-md-3'; $_colCount = 4; @endphp
						@endif

						@php
							$i++;
							$color01 = 'rgba(223,247,233,1)';
							$color02 = 'rgba(223,232,247,1)';
							$color03 = 'rgba(250,245,220,1)';
						@endphp
						
						@if ($i % ($_colCount * 3) == 1)
							@php $color00 = $color01; @endphp
						@elseif ($i % ($_colCount * 3) == ($_colCount + 1))
							@php $color00 = $color02; @endphp
						@elseif ($i % ($_colCount * 3) == ($_colCount + 2))
							@php $color00 = $color03; @endphp
						@endif

	    				@if ($i % $_colCount == 1)
	    					<div class="row" style="background-image: linear-gradient(rgba(255,0,0,0) 30%, {{ $color00 }} 30%); padding: 0 0 10px 0; margin-bottom: 20px;">
	    						<div class="container" style="padding: 0;">
									<div class="row">
				    					<div class="col-md-12">
				    	@endif

	    					<div class="{{ $_colValue }} news_list_panel2" style="padding: 15px;">
				    			<a class="group1 cboxElement" href="{{ url(env('APP_UPLOAD_PATH_V3').'large/'.$photo->url) }}" title="{{ $photo->name }}" style="color:#333">
				    				<div class="col-md-12" style="padding: 0;">
										<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$photo->url) }}">
									</div>
									@if ($photo->name != '')
										<div class="col-md-12" style="padding: 0;">
							    			<h5 style="color:#000000 !important;" class="eleHead">{{ $photo->name }}</h5>
							    			@if(!is_null($photo->description))
							    				<h5 style="margin-bottom: 0;">{{ $photo->description }}</h5>
							    			@endif
										</div>
									@endif
				    			</a>
					    	</div>

				    	@if ($loop->last || $i % $_colCount == 0)
				    					</div>
				    				</div>
				    			</div>
				    		</div>
				    	@endif

    				@endforeach
					
				</div>

			@endif

			@if ($content->type == 'group')
				
				<div class="container">
					<div class="row">

						@if ($content->variableLang(Request::segment(1))->title != '')
							<div class="col-md-9">
					    		<h2 class="contentHeader">{{ $content->variableLang(Request::segment(1))->title }}</h2>
				    		</div>
				    	@else
							<div class="col-md-9"> </div>
			    		@endif

			    		@if (!empty($content->release_date))
							<div class="col-md-3">
					    		<h5 style="float: right; margin-top: 35px; margin-bottom: 23px;">{{ Carbon::parse($content->release_date)->format('d.m.Y') }}</h5>
				    		</div>
				    	@else
							<div class="col-md-3"> </div>
			    		@endif
						
						@php $iltrFormat = 'no'; @endphp

						@if($content->sub_content->count() > 0)
							@if ($content->sub_content[0]->type == 'photo' && $content->sub_content[1]->type == 'text')
								@php $iltrFormat = 'yes'; @endphp
							@endif
						@endif
						
						@foreach ($content->sub_content as $element)
							@if ($element->type == 'photo')
								@if ($loop->first && $element->type == 'photo' && $iltrFormat == 'yes')
									<div class="col-md-4">
										<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'xlarge/'.$element->variableLang(Request::segment(1))->content) }}" alt="">
									</div>
								@endif
							@elseif($element->type == 'text')
								
								<div class="col-md-{{ ($loop->index == 1 && $iltrFormat == 'yes') ? '8' : '12' }}">
									{!! $element->variableLang(Request::segment(1))->content !!}
								</div>

							@elseif($element->type == 'photogallery')
								
								<div class="col-md-12">
									@php $_colValue = 'col-md-6';@endphp
									@if ($element->variableLang(Request::segment(1))->props == 'col-2x')
										@php $_colValue = 'col-md-6';@endphp
									@elseif($element->variableLang(Request::segment(1))->props == 'col-3x')
										@php $_colValue = 'col-md-4';@endphp
									@elseif($element->variableLang(Request::segment(1))->props == 'col-4x')
										@php $_colValue = 'col-md-3';@endphp
									@endif

				    				@foreach ($element->photogallery as $photo)
				    					
			    						<div class="{{ $_colValue }} news_list_panel2" style="padding: 15px;">
							    			<a class="group1 cboxElement" href="{{ url(env('APP_UPLOAD_PATH_V3').'large/'.$photo->url) }}" title="{{ $photo->name }}" style="color:#333">
							    				<div class="col-md-12" style="padding: 0;">
													<img class="img-responsive" src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$photo->url) }}">
												</div>
												<div class="col-md-12" style="padding: 0;">
									    			<h5 style="color:#000000 !important;" class="eleHead">{{ $photo->name }}</h5>
												</div>
							    			</a>
								    	</div>
								    	
				    				@endforeach

				    			</div>

							@endif

						@endforeach

					</div>
				</div>
				
			@endif

		@endif

	@endif	

	@if ($menu->variableLang(Request::segment(1))->slug != 'filli-bahcesi' && $menu->variableLang(Request::segment(1))->slug != 'kutuphanesi' && $menu->variableLang(Request::segment(1))->slug != 'iletisim')
		@include('partials.etkinlik')
	@endif

	@php
		$animRand = rand(0,1);
	@endphp

	@if ($animRand == 0)
		@include('animations.topac')
	@else
		@include('animations.cark')
	@endif
	
@endsection

@section('inline-scripts')
	<script type="text/javascript">

		var filterChange = false;

		function filterForm(type){
			if(filterChange){
				$('#filterForm').submit();
			}
		}

	    $(document).ready(function(){

	    	$('.iletisimFormCol').matchHeight();

	    	$('#resetButton').on('click', function (e) {
				$('#neredeFilter').select2("val", ['']);
				$('#neFilter').select2("val", ['']);
				$('#nezamanFilter').select2("val", ['']);
				filterForm('resetFilter');
			});

	    	$('#neredeFilter').select2({
                placeholder: "Nerede",
                //multiple: true,
    			//allowClear: true,
    			closeOnSelect: false,
    			language: {
		            noResults: function(term) {
		                 return "Eşleşme Yok";
		            }
			    }
            });
	    	
            $('#neredeFilter').on('select2:selecting', function (e) {
				if($(e.currentTarget).select2("val")[0] == 'all'){
					$(e.currentTarget).select2("val", ['']);
				}
			});

	    	$('#neredeFilter').on('change', function (e) {
	    		var value = $(e.currentTarget).select2("val");
	    		//console.log(value);
  				if(value[0] == 'all' && value.length > 1){
  					var value = $(e.currentTarget).select2("val", ["all"]);
  				}
	    		filterChange = true;
	    		$("#neredeFilterDiv .select2-search__field").attr("placeholder", "Nerede");
			});
			$("#neredeFilterDiv .select2-search__field").attr("placeholder", "Nerede");

            $('#neFilter').select2({
                placeholder: "Ne",
                //multiple: true,
    			//allowClear: false,
    			closeOnSelect: false,
    			language: {
		            noResults: function(term) {
		                 return "Eşleşme Yok";
		            }
			    }
            });

            $('#neFilter').on('select2:selecting', function (e) {
				if($(e.currentTarget).select2("val")[0] == 'all'){
					$(e.currentTarget).select2("val", ['']);
				}
			});

			$('#neFilter').on('change', function (e) {
	    		var value = $(e.currentTarget).select2("val");
	    		//console.log(value);
  				if(value[0] == 'all' && value.length > 1){
  					var value = $(e.currentTarget).select2("val", ["all"]);
  				}
	    		filterChange = true;
	    		$("#neFilterDiv .select2-search__field").attr("placeholder", "Ne");
			});
			$("#neFilterDiv .select2-search__field").attr("placeholder", "Ne");

            $('#nezamanFilter').select2({
                placeholder: "Ne Zaman",
                //multiple: true,
    			//allowClear: false,
    			closeOnSelect: false,
    			language: {
		            noResults: function(term) {
		                 return "Eşleşme Yok";
		            }
			    }
            });

            $('#nezamanFilter').on('select2:selecting', function (e) {
				if($(e.currentTarget).select2("val")[0] == 'all'){
					$(e.currentTarget).select2("val", ['']);
				}
			});

            $('#nezamanFilter').on('change', function (e) {
            	var value = $(e.currentTarget).select2("val");
	    		//console.log(value);
  				if(value[0] == 'all' && value.length > 1){
  					var value = $(e.currentTarget).select2("val", ["all"]);
  				}
	    		filterChange = true;
	    		$("#nezamanFilterDiv .select2-search__field").attr("placeholder", "Ne Zaman");
			});
			$("#nezamanFilterDiv .select2-search__field").attr("placeholder", "Ne Zaman");




            $('#neredeFilter').on('select2:close', function (e) {
	    		filterForm('neredeFilter');
			});

			$('#neFilter').on('select2:close', function (e) {
	    		filterForm('neFilter');
			});

			$('#nezamanFilter').on('select2:close', function (e) {
	    		filterForm('nezamanFilter');
			});



	    	$(".group1").colorbox({rel:'group1', maxWidth: '90%'});
	    	$('.news_list_panel2').matchHeight();
	    	$('.eleHead').matchHeight();
	    	$('.eleText').matchHeight();

	    	$('.owl-carousel').owlCarousel({
			    autoplay: false,
			    autoplayHoverPause: true,
			    loop: false,
			    dots: true,
	    		slideSpeed : 2000,
			    nav:false,
			    responsive:{
			        0:{
			            items:1
			        },
			        768:{
			            items:3
			        },
			        992:{
			            items:4
			        },
			        1200:{
			            items:6
			        }
			    }
	    	});
	    	
			@if ($menu->slidertype == "image" && $menu->variableLang(Request::segment(1))->stvalue != null)
			@else
				ucakbulutanim();
			@endif

	    	@if (isset($event))
		    	@if(Carbon::parse($event->start)->greaterThan(Carbon::now('Europe/Istanbul')))
					@if (!is_null($event->reservationForm))
						@if ( ($event->capacity > 0 && $event->reservationFormData->count() < $event->capacity) || $event->capacity == 0 || $event->capacity == -1)

			    		$('#form_element_{{ $event->reservationForm->id }}').formRender({
							dataType: 'json',
							formData: formData_{{ $event->id }}
						});

						$.extend($.validator.messages, {
						    required: "Bu alanı doldurmak zorunludur."
						});

						$('#form_submit_{{ $event->reservationForm->id }}').click(function(e) {

				            e.preventDefault();
				            var btn = $(this);
				            var form = $(this).closest('form');

				            form.validate();

				            if (!form.valid()) {
				                return;
				            }

				            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
				            form.submit();
				        });

			            @endif
			        @endif
		    	@endif
		    @endif

			@foreach ($menu->content as $element)
				@if ($element->type == 'form')
					
					$('#form_element_{{ $element->id }}').formRender({
						dataType: 'json',
						formData: formData_{{ $element->id }}
					});

					$.extend($.validator.messages, {
					    required: "Bu alanı doldurmak zorunludur."
					});

					$('#form_submit_{{ $element->id }}').click(function(e) {

			            e.preventDefault();
			            var btn = $(this);
			            var form = $(this).closest('form');

			            form.validate();

			            if (!form.valid()) {
			                return;
			            }

			            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
			            form.submit();
			        });

				@endif
			@endforeach

	    	@if ($menu->type == 'calendar')
	    		
				$('#calendarScript').fullCalendar({
		    		header: {
		                left: '',
		                center: 'title',
		                right: 'prev,next,today,month,listMonth'
		            },
		            themeSystem: 'bootstrap3',
		            locale: 'tr',
		            displayEventEnd: true,
		            url: false,
		            @php

		            	if (Session::has('d')) {
		            		echo "defaultDate: '".Session::get('d')."',";
		            	}

		            @endphp
		            timeFormat: 'H:mm',
		            events: [
		                @foreach ($calendar as $element)
		                	
		               	
		                	@php $isVisibleC = true; @endphp
		                	@if ($element->capacity == -1 && Carbon::parse($element->start)->startOfDay()->addDay()->greaterThan(Carbon::now()->startOfDay()) )
		                		@php $isVisibleC = false; @endphp
		                	@endif

		                	@if ($isVisibleC)
			                	@php $isVisible = false; @endphp
								@if (!session()->has('filter.c') && !session()->has('filter.t'))
									@php $isVisible = true; @endphp
								@elseif (session()->has('filter.c') && $element->categories->where('id', session()->get('filter.c'))->count() > 0)
									@php $isVisible = true; @endphp
								@elseif (session()->has('filter.t') && $element->tags->where('id', session()->get('filter.t'))->count() > 0)
									@php $isVisible = true; @endphp
								@endif

				                @php

				                    if ( Carbon::parse($element->start)->startOfDay() == Carbon::now()->startOfDay() ) {
				                    	$bgcolor = '#FFFFFF';
				                    	$timePTF = 'fc-event-today';
				                    }else if ( Carbon::parse($element->start)->startOfDay() < Carbon::now()->startOfDay() ) {
				                    	$bgcolor = '#9d9d9c';
				                    	$timePTF = 'fc-event-past';
				                    }else if ( Carbon::parse($element->start)->startOfDay() > Carbon::now()->startOfDay() ) {
				                    	$bgcolor = '#fecf00';
				                    	$timePTF = 'fc-event-future';
				                    } 

				                    $icon = '';
				                    if($element->type == 'filmgosterimi'){
			                            $icon = 'cicon icon-film';
			                        }else if($element->type == 'konser'){
			                            $icon = 'cicon icon-konser';
			                        }else if($element->type == 'kutuphaneetkinligi'){
			                            $icon = 'cicon icon-kutuphane';
			                        }else if($element->type == 'muzeetkinligi'){
			                            $icon = 'cicon icon-muze';
			                        }else if($element->type == 'soylesi'){
			                            $icon = 'cicon icon-soylesi';
			                        }


				                    $kapasiteee = '';
					                if ($element->capacity == -1) {
					                	$kapasiteee = 'Kapalı Grup';
					                }elseif($element->capacity == 0){
					                	$kapasiteee = 'Kapasite: Sınırsız';
					                }else{
					                	$kapasiteee = 'Kapasite: '.$element->capacity.' Kişi';
					                }

					                $ucrettt = '';
					                if ($element->price == 0) {
					                	$ucrettt = 'Ücretsiz Etkinlik';
					                }else{
					                	$ucrettt = 'Ücret: '.$element->price.' ₺';
					                }

				                @endphp

			                {
			                    id: '{{$element->id}}',
			                    title: "{!! $element->variable->title !!}",
			                    icon : '{{$icon}}',
			                    timePTF: '{{$timePTF}}',
			                    start: '{{$element->start}}',
			                    end: '{{$element->end}}',
			                    url: '{{ url($lang.'/'.$slug.'/'.$element->id) }}',
			                    editable: '{{--$element->editable--}}',
			                    color: '{{$bgcolor}}',
			                    image: '{!! is_null($element->variable->image_name) ? '' : '<img src="'.url(env('APP_UPLOAD_PATH_V3').'small/'.$element->variable->image_name).'" width="100%" /><br>' !!}'
			                },
			                @endif
		                @endforeach
		            ],
		            eventRender: function(event, element) {

		            	$(element).toggleClass(event.timePTF);
		            	
		            	if(event.icon){          
					        element.find(".fc-title").html("<i class='"+event.icon+"'></i>");
					    };

		                element.popover({
		                    container: 'body',
		                    html: true,
		                    placement: 'bottom',
		                    title: '<strong>'+event.title+'</strong>',
		                    content: event.image,
		                    trigger: 'hover',
		                });
		            }
		        });

			@endif

			@if ($animRand == 0)
				topacanim();
			@else
				carkanim();
			@endif
			
		});
	</script>
@endsection