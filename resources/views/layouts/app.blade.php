<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <title>{{ $sitesettings->where('slug', 'seo-title')->first()->value }} {{-- $menu->title --}}</title>

    <meta name="title" content="{{ $sitesettings->where('slug', 'seo-title')->first()->value }}">
    <meta name="description" content="{{ $sitesettings->where('slug', 'seo-description')->first()->value }}">
    <meta name="keywords" content="{{ $sitesettings->where('slug', 'seo-keyword')->first()->value }}">

    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    
    @if ($headerslug != '')
        @if ($headerslug == 'muzesi')
            <link href="{{ mix('css/muze.css') }}" rel="stylesheet">
        @endif
        @if ($headerslug == 'vakfi')
            <link href="{{ mix('css/vakif.css') }}" rel="stylesheet">
        @endif
        @if ($headerslug == 'kutuphanesi')
            <link href="{{ mix('css/kutuphane.css') }}" rel="stylesheet">
        @endif
        @if ($headerslug == 'filli-bahcesi')
            <link href="{{ mix('css/fillibahce.css') }}" rel="stylesheet">
        @endif
    @endif

</head>
<body>
    
    @include('partials.header')
    @yield('content')
    @include('partials.footer')
    @include('partials.socialmedia')

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/plugins/ScrollToPlugin.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js"></script>

    @yield('inline-scripts')
    
    <script type="text/javascript">
        $('#form_element_216').formRender({
            dataType: 'json',
            formData: formData_216
        });
        @if(Session::get('message'))
            $(function() {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr.{!! Session::get('message')['status'] !!}('{{Session::get('message')['text']}}')
            });
        @endif
    </script>
    {!! $sitesettings->where('slug', 'google-analytics-code')->first()->value !!}
</body>
</html>
