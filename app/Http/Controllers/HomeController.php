<?php

namespace App\Http\Controllers;

use Auth;
use Redirect;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\User;
use App\Menu;
use App\MenuVariable;
use App\Content;
use App\ContentVariable;

use App\SiteSettings;
use App\Tag;
use App\TagVariable;
use App\Category;
use App\CategoryVariable;

use App\Calendar;
use App\CalendarVariable;

use App\FormData;

use App\MenuHasContent;
use App\ContentHasTag;
use App\ContentHasCategory;

use Illuminate\Support\Arr;

class HomeController extends Controller
{

    public function index($lang, $slug, $attr = null, $param = null)
    {
            //dd($slug);
        //DB::enableQueryLog();
        //dd(DB::getQueryLog());
        if(isset($_GET['c'])){
            session()->flash('filter.c', $_GET['c']);
        }else if(isset($_GET['t'])){
            session()->flash('filter.t', $_GET['t']);
        }else{
            session()->flash('filter', '');
        }

        $menu = MenuVariable::where('slug', $slug)->first()->menu;
        $headerslug = $menu;
        while ($headerslug->top_id != null) {
            $headerslug = $headerslug->topMenu;
        }

        $headerslug = $headerslug->variables->where('lang_code', $lang)->first()->slug;
        //echo $headerslug;
        $menuall = Menu::where('top_id', null)->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $mainmenu = Menu::where(
            function ($query){
                $query->where('position', 'all')->orWhere('position', 'top');
            }
        )->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();

        $topmenus = Menu::where('position', '!=', 'aside')->where('top_id', null)->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $asidemenus = Menu::where('position', '!=', 'top')->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

        $sitesettings = SiteSettings::all();

        $startDate = Carbon::now(); //->copy()->startOfDay();
    
        $calendarSlide = Calendar::where('start', '>', $startDate)->where('capacity', '!=', -1)->where('deleted', 'no')->where('status', 'active')->orderBy('start', 'asc')->get();
        //$calendarSlide = Calendar::where('capacity', '!=', -1)->where('deleted', 'no')->where('status', 'active')->orderBy('start', 'desc')->get();

        $calendar = Calendar::where('deleted', 'no')->where('status', 'active')->orderBy('start', 'asc')->get();

        $tags = Tag::where('deleted', 'no')->where('status', 'active')->get();
        $tagIds = array();
        if($menu->type == 'calendar'){
            foreach ($calendar as $cald) {
                foreach ($cald->tags()->get() as $tagg) {
                    $tagIds[] = $tagg->id;
                }
            }
        }else{
            foreach ($menu->content()->get() as $cont) {
                foreach ($cont->tags()->get() as $tagg) {
                    $tagIds[] = $tagg->id;
                }
            }
        }
        $tagsAvailable = Tag::whereIn('id', $tagIds)->where('deleted', 'no')->where('status', 'active')->get();

        $categories = Category::where('deleted', 'no')->where('status', 'active')->get();
        $categoryIds = array();
        if($menu->type == 'calendar'){
            foreach ($calendar as $cald) {
                foreach ($cald->categories()->get() as $catt) {
                    $categoryIds[] = $catt->id;
                }
            }
        }else{
            foreach ($menu->content()->get() as $cont) {
                foreach ($cont->categories()->get() as $catt) {
                    $categoryIds[] = $catt->id;
                }
            }
        }

        $categoriesAvailable = Category::whereIn('id', $categoryIds)->where('deleted', 'no')->where('status', 'active')->get();

        if ($slug == 'etkinlikler') {
            $content = null;
        }else{
            $content = ContentVariable::where('slug', $attr)->first()->tcontent;
        }

        return view('home', array('menu' => $menu, 'menuall' => $menuall, 'mainmenu' => $mainmenu, 'topmenus' => $topmenus, 'asidemenus' => $asidemenus, 'sitesettings' => $sitesettings, 'calendar' => $calendar, 'calendarSlide' => $calendarSlide, 'tags' => $tags, 'tagsAvailable' => $tagsAvailable, 'categories' => $categories, 'categoriesAvailable' => $categoriesAvailable, 'headerslug' => $headerslug, 'lang' => $lang, 'slug' => $slug, 'attr' => $attr, 'param' => $param, 'content' => $content));

    }

    public function form_save(Request $request)
    {
        //dump($request->input());
        //dd($request->url());
        $jsonEncodeReq = json_encode($request->input());
        $jsonDecodeReq = json_decode($jsonEncodeReq, true);

        //dd(json_encode($jsonDecodeReq));

        if ($request->source_type == 'calendar') {

            $calendar = Calendar::find($request->source_id);

            if (!is_null($calendar->price) && $calendar->price > 0) {
                echo 'Ödeme Sayfası';
            }else{
                Arr::pull($jsonDecodeReq, '_token');
                Arr::pull($jsonDecodeReq, 'lang');
                Arr::pull($jsonDecodeReq, 'source_type');
                Arr::pull($jsonDecodeReq, 'source_id');
                Arr::pull($jsonDecodeReq, 'form_id');

                $formdata = new FormData();
                $formdata->lang_code = $request->lang;
                $formdata->source_type = $request->source_type;
                $formdata->source_id = $request->source_id;
                $formdata->form_id = $request->form_id;
                $formdata->data = json_encode($jsonDecodeReq);
                $formdata->save();

                return back()->with('message', array('text' => 'Form Başarıyla Kaydedildi.', 'status' => 'success'));
            }

        } else {

            Arr::pull($jsonDecodeReq, '_token');
            Arr::pull($jsonDecodeReq, 'lang');
            Arr::pull($jsonDecodeReq, 'source_type');
            Arr::pull($jsonDecodeReq, 'source_id');
            Arr::pull($jsonDecodeReq, 'form_id');

            //dd(json_encode($jsonDecodeReq));

            $formdata = new FormData();
            $formdata->lang_code = $request->lang;
            $formdata->source_type = $request->source_type;
            $formdata->source_id = $request->source_id;
            $formdata->form_id = $request->form_id;
            $formdata->data = json_encode($jsonDecodeReq);
            $formdata->save();

            return back()->with('message', array('text' => 'Form Başarıyla Kaydedildi.', 'status' => 'success'));
            
        }
    }

}
