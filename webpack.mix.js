let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.combine([
	'node_modules/jquery/dist/jquery.min.js',
	'node_modules/bootstrap/dist/js/bootstrap.min.js',
	'node_modules/ladda/dist/spin.min.js',
	'node_modules/ladda/dist/ladda.min.js',
	'node_modules/owl.carousel/dist/owl.carousel.min.js',
	'node_modules/jquery-colorbox/jquery.colorbox.js',

	'node_modules/toastr/build/toastr.min.js',
	'node_modules/select2/dist/js/select2.js',

    'node_modules/moment/min/moment.min.js',
    'node_modules/moment/locale/tr.js',

	'node_modules/fullcalendar/dist/fullcalendar.js',
    'node_modules/fullcalendar/dist/locale/tr.js',

	'resources/assets/js/ion.rangeSlider.min.js',
	'resources/assets/js/jquery.matchHeight-min.js',

	'node_modules/jquery-ui-sortable/jquery-ui.min.js',
	'node_modules/formBuilder/dist/form-render.min.js',
	'node_modules/jquery-validation/dist/jquery.validate.min.js',

    'resources/assets/js/custom.js',
], 'public/js/app.js').version();


mix.combine([
	'node_modules/bootstrap/dist/css/bootstrap.min.css',
	'node_modules/font-awesome/css/font-awesome.min.css',
	'node_modules/ladda/dist/ladda.min.css',
	'node_modules/ladda/dist/ladda-themeless.min.css',
	'node_modules/owl.carousel/dist/assets/owl.carousel.min.css',
	'node_modules/jquery-colorbox/example2/colorbox.css',

	'node_modules/toastr/build/toastr.min.css',
	'node_modules/select2/dist/css/select2.css',

	'node_modules/fullcalendar/dist/fullcalendar.css',

	'resources/assets/css/cinali-icon.css',
	'resources/assets/css/ion.rangeSlider.css',
    'resources/assets/css/ion.rangeSlider.skinFlat.css',
    'resources/assets/css/custom.css',
    'resources/assets/css/fullcalendarcustom.css',
], 'public/css/app.css').version();

mix.copy([
	'resources/assets/css/muze_custom.css',
], 'public/css/muze.css', false).version();

mix.copy([
	'resources/assets/css/vakif_custom.css',
], 'public/css/vakif.css', false).version();

mix.copy([
	'resources/assets/css/kutuphane_custom.css',
], 'public/css/kutuphane.css', false).version();

mix.copy([
	'resources/assets/css/fillibahce_custom.css',
], 'public/css/fillibahce.css', false).version();

mix.copy([
	'node_modules/bootstrap/dist/fonts/',
	'node_modules/font-awesome/fonts/',
], 'public/fonts', false).version();

mix.copy('resources/assets/fonts/cinali', 'public/fonts');
mix.copy('resources/assets/fonts/cinali-icon', 'public/fonts');

mix.copy('resources/assets/images', 'public/images');
mix.copy('resources/assets/favicon.ico', 'public/favicon.ico');

mix.copy('resources/assets/svg', 'public/svg');

mix.copy('node_modules/jquery-colorbox/example2/images', 'public/css/images');

